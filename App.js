/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import 'react-native-gesture-handler';
import React from 'react';
import {YellowBox,View,Text} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {enableScreens} from 'react-native-screens';
import {Provider} from 'react-redux';
import {store, persistor} from './src/stores';
import {PersistGate} from 'redux-persist/integration/react';

enableScreens();

// TODO: Remove when fixed
YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested',
  'Warning: componentWillReceiveProps has been renamed, and is not recommended',
  'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
]);

// import MainNavigatorA or MainNavigatorB to preview design differnces
import MainNavigator from './src/navigation/MainNavigatorA';

// APP
function App() {
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <PersistGate loading={<View><Text>Loding....</Text></View>} persistor={persistor}>
          <MainNavigator />
        </PersistGate>
      </SafeAreaProvider>
    </Provider>
  );
}

export default App;
