import {API_URL} from '../../../api/env.json';
import * as RootNavigation from '../../../navigation/RootNavigation';
import axios from 'axios';

export const fetchProfileRequest = () => {
  return {
    type: 'FETCH_PROFILE_REQUEST',
  };
};

export const fetchProfileSuccess = (profile, params) => {
  return {
    type: 'FETCH_PROFILE_SUCCESS',
    payload: profile,
    params,
  };
};

export const fetchProfileFail = () => {
  return {
    type: 'FETCH_PROFILE_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataProfile = profile => dispatch => {
  dispatch(fetchProfileRequest(profile));
  return new Promise(() => {
    axios
      .get(`${API_URL}profile`)
      .then(response => {
        dispatch(fetchProfileSuccess(response.data, profile));
        // RootNavigation.navigate('Profile');
      })
      .catch(error => {
        dispatch(fetchProfileFail(error));
        console.log(error);
      });
  });
};
