import {API_URL} from '../../../api/env.json';
import axios from 'axios';
import * as RootNavigation from '../../../navigation/RootNavigation';
import {fetchDataProductSearch} from '../../actions/product/search.action';

export const fetchDistributorDetailRequest = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_DETAIL_REQUEST',
  };
};

export const fetchDistributorDetailSuccess = (distributor, params) => {
  return {
    type: 'FETCH_DISTRIBUTOR_DETAIL_SUCCESS',
    payload: distributor,
    params,
  };
};

export const fetchDistributorDetailFail = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_DETAIL_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataDistributorDetail = distributor => dispatch => {
  dispatch(fetchDistributorDetailRequest(distributor));
  return new Promise(() => {
    axios
      .get(`${API_URL}distributor/index?`, {params: distributor})
      .then(response => {
        dispatch(fetchDistributorDetailSuccess(response.data, distributor));
        console.log('DIST DDETAIL', response.data);
        dispatch(
          fetchDataProductSearch({
            distributor_id: Number(response.data.data.id),
            page: 1,
            per_page: 100,
          }),
        );
        RootNavigation.navigate('DistributorDetail');
      })
      .catch(error => {
        dispatch(fetchDistributorDetailFail(error));
        console.log(error);
      });
  });
};
