import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchDistributorRegionRequest = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_REGION_REQUEST',
  };
};

export const fetchDistributorRegionSuccess = (city) => {
  return {
    type: 'FETCH_DISTRIBUTOR_REGION_SUCCESS',
    payload: city,
  };
};

export const fetchDistributorRegionFail = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_REGION_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataDistributorRegion = () => (dispatch) => {
  //   console.log('subdistrict_id', subdistrict_id);
  dispatch(fetchDistributorRegionRequest());
  return new Promise(() => {
    axios
      .get(`${API_URL}distributor/region`)
      .then((response) => {
        dispatch(fetchDistributorRegionSuccess(response.data));
        // console.log('fetchDataDistributorRegionSuccess', response);
      })
      .catch((error) => {
        dispatch(fetchDistributorRegionFail(error));
        console.log(error);
      });
  });
};
