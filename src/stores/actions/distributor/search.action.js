import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchDistributorSearchRequest = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_SEARCH_REQUEST',
  };
};

export const fetchDistributorSearchSuccess = (distributor, params) => {
  return {
    type: 'FETCH_DISTRIBUTOR_SEARCH_SUCCESS',
    payload: distributor,
    params,
  };
};

export const fetchDistributorSearchFail = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_SEARCH_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataDistributorSearch = distributor => dispatch => {
  dispatch(fetchDistributorSearchRequest(distributor));
  return new Promise(() => {
    axios
      .get(`${API_URL}distributor/search?`, {params: distributor})
      .then(response => {
        dispatch(fetchDistributorSearchSuccess(response.data, distributor));
      })
      .catch(error => {
        dispatch(fetchDistributorSearchFail(error));
        console.log(error);
      });
  });
};
