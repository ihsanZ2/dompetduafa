import {API_URL} from '../../../api/env.json';
// import axios from 'axios';
import * as RootNavigation from '../../../navigation/RootNavigation';
import axios from 'axios';
import Toast from '../../../components/toast/Toast';

export const fetchDistributorSearchByLokasiRequest = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_SEARCH_BY_LOKASI_REQUEST',
  };
};

export const fetchDistributorSearchByLokasiSuccess = (distributor, params) => {
  return {
    type: 'FETCH_DISTRIBUTOR_SEARCH_BY_LOKASI_SUCCESS',
    payload: distributor,
    params,
  };
};

export const fetchDistributorSearchByLokasiFail = () => {
  return {
    type: 'FETCH_DISTRIBUTOR_SEARCH_BY_LOKASI_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataDistributorSearchByLokasi = distributor => dispatch => {
  dispatch(fetchDistributorSearchByLokasiRequest(distributor));
  return new Promise(() => {
    axios
      .get(`${API_URL}distributor/search?`, {params: distributor})
      .then(response => {
        dispatch(
          fetchDistributorSearchByLokasiSuccess(response.data, distributor),
        );
        if (response.data.data.length > 0) {
          RootNavigation.navigate('DistributorRegion');
        } else {
          Toast('Data Tidak di Temukan');
        }
      })
      .catch(error => {
        dispatch(fetchDistributorSearchByLokasiFail(error));
        console.log(error);
      });
  });
};
