import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchKelurahanRequest = () => {
  return {
    type: 'FETCH_KELURAHAN_REQUEST',
  };
};

export const fetchKelurahanSuccess = (city) => {
  return {
    type: 'FETCH_KELURAHAN_SUCCESS',
    payload: city,
  };
};

export const fetchKelurahanFail = () => {
  return {
    type: 'FETCH_KELURAHAN_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataKelurahan = (subdistrict_id) => (dispatch) => {
  //   console.log('subdistrict_id', subdistrict_id);
  dispatch(fetchKelurahanRequest());
  return new Promise(() => {
    axios
      .get(`${API_URL}region/kelurahan?`, {
        params: subdistrict_id,
      })
      .then((response) => {
        dispatch(fetchKelurahanSuccess(response.data));
        // console.log('fetchDataKelurahanSuccess', response);
      })
      .catch((error) => {
        dispatch(fetchKelurahanFail(error));
        console.log(error);
      });
  });
};
