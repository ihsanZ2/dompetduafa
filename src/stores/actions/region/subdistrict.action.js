import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchSubdistrictRequest = () => {
  return {
    type: 'FETCH_SUBDISTRICT_REQUEST',
  };
};

export const fetchSubdistrictSuccess = (city) => {
  return {
    type: 'FETCH_SUBDISTRICT_SUCCESS',
    payload: city,
  };
};

export const fetchSubdistrictFail = () => {
  return {
    type: 'FETCH_SUBDISTRICT_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataSubdistrict = (city_id) => (dispatch) => {
  // console.log('city_id', city_id);
  dispatch(fetchSubdistrictRequest());
  return new Promise(() => {
    axios
      .get(`${API_URL}region/subdistrict?`, {params: city_id})
      .then((response) => {
        dispatch(fetchSubdistrictSuccess(response.data));
        // console.log('fetchDataSubdistrictSuccess', response);
      })
      .catch((error) => {
        dispatch(fetchSubdistrictFail(error));
        console.log(error);
      });
  });
};
