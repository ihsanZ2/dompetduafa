import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchProvinceRequest = () => {
  return {
    type: 'FETCH_PROVINCE_REQUEST',
  };
};

export const fetchProvinceSuccess = (province) => {
  return {
    type: 'FETCH_PROVINCE_SUCCESS',
    payload: province,
  };
};

export const fetchProvinceFail = () => {
  return {
    type: 'FETCH_PROVINCE_FAILED',
  };
};

export const fetchDataProvince = () => (dispatch) => {
  dispatch(fetchProvinceRequest());
  return new Promise(() => {
    axios
      .get(`${API_URL}region/province`)
      .then((response) => {
        dispatch(fetchProvinceSuccess(response.data));
        // console.log('fetchDataProvinceSuccess', response);
      })
      .catch((error) => {
        dispatch(fetchProvinceFail(error));
        console.log(error);
      });
  });
};
