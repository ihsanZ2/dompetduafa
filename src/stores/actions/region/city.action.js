import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchCityRequest = () => {
  return {
    type: 'FETCH_CITY_REQUEST',
  };
};

export const fetchCitySuccess = (city, params) => {
  return {
    type: 'FETCH_CITY_SUCCESS',
    payload: city,
    params,
  };
};

export const fetchCityFail = () => {
  return {
    type: 'FETCH_CITY_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataCity = (province_id) => (dispatch) => {
  dispatch(fetchCityRequest(province_id));
  return new Promise(() => {
    axios
      .get(`${API_URL}region/cities?`, {params: province_id})
      .then((response) => {
        dispatch(fetchCitySuccess(response.data, province_id));
        
      })
      .catch((error) => {
        dispatch(fetchCityFail(error));
        console.log(error);
      });
  });
};
