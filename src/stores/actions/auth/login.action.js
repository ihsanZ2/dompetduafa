import {API_URL} from '../../../api/env.json';
import axios from 'axios';
import * as RootNavigation from '../../../navigation/RootNavigation';
import {fetchDataProfile} from '../profile/profile.action';
import Toast from '../../../components/toast/Toast';

export const fetchLoginRequest = () => {
  return {
    type: 'FETCH_LOGIN_REQUEST',
  };
};

export const fetchLoginSuccess = (login, params) => {
  return {
    type: 'FETCH_LOGIN_SUCCESS',
    payload: login,
    params,
  };
};

export const fetchLoginFail = () => {
  return {
    type: 'FETCH_LOGIN_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataLogin = login => dispatch => {
  console.log('logins', login);
  dispatch(fetchLoginRequest(login));
  return new Promise(() => {
    axios
      .post(`${API_URL}profile/login`, login)
      .then(response => {
        dispatch(fetchLoginSuccess(response.data, login));
        RootNavigation.navigate('Home');
        dispatch(fetchDataProfile());
      })
      .catch(error => {
        dispatch(fetchLoginFail(error.response));
        Toast(`${error.response.data.message}`);
        console.log('error', error.response);
      });
  });
};
