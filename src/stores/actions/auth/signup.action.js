import {API_URL} from '../../../api/env.json';
import axios from 'axios';
import * as RootNavigation from '../../../navigation/RootNavigation';

export const fetchSignUpRequest = () => {
  return {
    type: 'FETCH_SIGNUP_REQUEST',
  };
};

export const fetchSignUpSuccess = (signup, params) => {
  return {
    type: 'FETCH_SIGNUP_SUCCESS',
    payload: signup,
    params,
  };
};

export const fetchSignUpFail = () => {
  return {
    type: 'FETCH_SIGNUP_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchSignUp = signup => dispatch => {
  console.log('signups', signup);
  dispatch(fetchSignUpRequest(signup));
  return new Promise(() => {
    axios
      .post(`${API_URL}profile/register`, signup)
      .then(response => {
        dispatch(fetchSignUpSuccess(response.data, signup));
        RootNavigation.navigate('SignIn');
      })
      .catch(error => {
        dispatch(fetchSignUpFail(error));
        console.log(error);
      });
  });
};
