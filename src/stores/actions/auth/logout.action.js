import {API_URL} from '../../../api/env.json';
import axios from 'axios';
import * as RootNavigation from '../../../navigation/RootNavigation';

export const fetchLogoutRequest = () => {
  return {
    type: 'FETCH_LOGOUT_REQUEST',
  };
};

export const fetchLogoutSuccess = users => {
  return {
    type: 'FETCH_LOGOUT_SUCCESS',
    payload: users,
  };
};

export const fetchLogoutFail = () => {
  return {
    type: 'FETCH_LOGOUT_FAILED',
  };
};

export const deleteToken = () => {
  return {
    type: 'FETCH_LOGIN_DELETE',
  };
};

export const deleteProfile = () => {
  return {
    type: 'FETCH_PROFILE_DELETE',
  };
};

export const fetchLogoutUser = () => dispatch => {
  dispatch(fetchLogoutRequest());
  return new Promise(() => {
    axios
      .get(`${API_URL}profile/logout`)
      .then(response => {
        dispatch(fetchLogoutSuccess(response.data));
        dispatch(deleteToken());
        // axios.defaults.headers.common.Authorization = 'aan';
        RootNavigation.navigate('Home');
      })
      .catch(error => {
        dispatch(fetchLogoutFail(error));
        dispatch(deleteToken());
        // dispatch(deleteProfile());
        console.log(error);
      });
  });
};
