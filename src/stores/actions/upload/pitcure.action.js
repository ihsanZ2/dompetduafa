import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchUploadRequest = () => {
  return {
    type: 'FETCH_UPLOAD_REQUEST',
  };
};

export const fetchUploadSuccess = (upload, params) => {
  return {
    type: 'FETCH_UPLOAD_SUCCESS',
    payload: upload,
    params,
  };
};

export const fetchUploadFail = () => {
  return {
    type: 'FETCH_UPLOAD_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchUpload = upload => dispatch => {
  dispatch(fetchUploadRequest(upload));
  return new Promise(() => {
    axios
      .post(`${API_URL}profile/upload`, upload)
      .then(response => {
        console.log('response.data', response.data.message);
        dispatch(fetchUploadSuccess(response.data.message, upload));
      })
      .catch(error => {
        dispatch(fetchUploadFail(error));
        console.log(error);
      });
  });
};

export const fotoKtpRequest = () => {
  return {
    type: 'FOTO_KTP_REQUEST',
  };
};

export const fotoKtpSuccess = (upload, params) => {
  return {
    type: 'FOTO_KTP_SUCCESS',
    payload: upload,
    params,
  };
};

export const fotoKtpFail = () => {
  return {
    type: 'FOTO_KTP_FAILED',
  };
};

export const fetchKtpUpload = upload => dispatch => {
  dispatch(fotoKtpRequest(upload));
  return new Promise(() => {
    axios
      .post(`${API_URL}profile/upload`, upload)
      .then(response => {
        console.log('response.data', response.data.message);
        dispatch(fotoKtpSuccess(response.data.message, upload));
      })
      .catch(error => {
        dispatch(fotoKtpFail(error));
        console.log(error);
      });
  });
};
