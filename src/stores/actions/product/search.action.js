import {API_URL} from '../../../api/env.json';
import axios from 'axios';
import Toast from '../../../components/toast/Toast';

export const fetchProductSearchRequest = () => {
  return {
    type: 'FETCH_PRODUCT_SEARCH_REQUEST',
  };
};

export const fetchProductSearchSuccess = (product_search, params) => {
  return {
    type: 'FETCH_PRODUCT_SEARCH_SUCCESS',
    payload: product_search,
    params,
  };
};

export const fetchProductSearchFail = () => {
  return {
    type: 'FETCH_PRODUCT_SEARCH_FAILED',
  };
};

export const deleteProductSearch = () => {
  return {
    type: 'DELETE_PRODUCT_SEARCH',
  };
};

// eslint-disable-next-line no-undef
export const fetchDataProductSearch = product_search => dispatch => {
  dispatch(fetchProductSearchRequest(product_search));
  return new Promise(() => {
    axios
      .get(`${API_URL}product/search?`, {params: product_search})
      .then(response => {
        dispatch(fetchProductSearchSuccess(response.data, product_search));
        // console.log('fetchDataProductSearchSuccess', response);
      })
      .catch(error => {
        dispatch(fetchProductSearchFail(error));
        Toast(`${error.response.data.message}`);
        console.log(error);
      });
  });
};
