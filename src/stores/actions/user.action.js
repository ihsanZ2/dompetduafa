import {getUser} from '../../api/fakeApiUser';
// import {store} from '../../stores'

export const fetchUserRequest = () => {
  return {
    type: 'FETCH_USER_REQUEST',
  };
};

export const fetchUserSuccess = (users) => {
  return {
    type: 'FETCH_USER_SUCCESS',
    payload: users,
  };
};

export const fetchUserFail = () => {
  return {
    type: 'FETCH_USER_FAILED',
  };
};

export const fetchDataUser = (arg) => async (dispatch) => {
  // const state = store.getState();
  console.log('store',state.loginReducer.login.data)
  try {
    dispatch(fetchUserRequest());
    const {data} = await getUser();
    dispatch(fetchUserSuccess(data));
    console.log('fetchDataUser', arg);
  } catch (error) {
    dispatch(fetchUserFail());
  }
};
