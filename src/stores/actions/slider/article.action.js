import {API_URL} from '../../../api/env.json';
import axios from 'axios';

export const fetchArticleRequest = () => {
  return {
    type: 'FETCH_ARTICLE_REQUEST',
  };
};

export const fetchArticleSuccess = (article) => {
  return {
    type: 'FETCH_ARTICLE_SUCCESS',
    payload: article,
  };
};

export const fetchArticleFail = () => {
  return {
    type: 'FETCH_ARTICLE_FAILED',
  };
};

export const fetchDataArticle = () => (dispatch) => {
  dispatch(fetchArticleRequest());
  return new Promise(() => {
    axios
      .get(`${API_URL}article/list`)
      .then((response) => {
        dispatch(fetchArticleSuccess(response.data));
        // console.log('fetchDataArticleSuccess', response);
      })
      .catch((error) => {
        dispatch(fetchArticleFail(error));
        console.log(error);
      });
  });
};
