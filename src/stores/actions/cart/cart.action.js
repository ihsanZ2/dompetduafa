import {API_URL} from '../../../api/env.json';
import axios from 'axios';
import * as RootNavigation from '../../../navigation/RootNavigation';
import {fetchDataProductSearch} from '../../actions/product/search.action';
import Toast from '../../../components/toast/Toast';

export const cartRequest = () => {
  return {
    type: 'CART_DETAIL_REQUEST',
  };
};

export const cartSuccess = (cart, params) => {
  return {
    type: 'CART_DETAIL_SUCCESS',
    payload: cart,
    params,
  };
};

export const cartFail = () => {
  return {
    type: 'CART_DETAIL_FAILED',
  };
};

// eslint-disable-next-line no-undef
export const fetchCart = cart => dispatch => {
  dispatch(cartRequest(cart));
  return new Promise(() => {
    axios
      .post(`${API_URL}order/add-to-cart`, cart)
      .then(response => {
        dispatch(cartSuccess(response.data, cart));
        console.log('cart', response.data.data);
        RootNavigation.navigate('WebViewArticle', {
          url: response.data.data,
        });
        // RootNavigation.navigate('');
      })
      .catch(error => {
        dispatch(cartFail(error));
        if (error.response.status == 401) {
          RootNavigation.navigate('Welcome');
          Toast('Anda Belum Login');
        } else if (error.response.status == 400) {
          Toast('Pilih Produk Dahulu');
        } else {
          Toast(error.response.data.message);
        }
        console.log(error.response);
      });
  });
};
