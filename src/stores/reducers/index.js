import {combineReducers} from 'redux';
import userReducer from './user.reducer';
import provinceReducer from './region/province.reducer';
import cityReducer from './region/city.reducer';
import subdistrictReducer from './region/subdictrict.reducer';
import kelurahanReducer from './region/kelurahan.reducer';
import distributorRegionReducer from './distributor/region.reduser';
import articleReducer from './slider/article.reducer';
import product_searchReducer from './product/search.reducer';
import distributorSearchReducer from './distributor/search.reducer';
import distributorSearchByLokasiReducer from './distributor/searchByLokasi.reducer';
import distributorDetailReducer from './distributor/detail.reducer';
import loginReducer from './auth/login.reducer';
import pitcutereReducer from './upload/pitcure.reducer';
import signUpReducer from './auth/signup.reducer';
import logoutReducer from './auth/logout.reducer';
import fotoKtpReducer from './upload/ktp.reducer';
import profileReducer from './profile/profile.reducer';
import cartReducer from './cart/cart.reducer';
import firstInstallReducer from './initial/firstInstall.reducer';
//insert another reducers here to be combined

const reducers = combineReducers({
  userReducer,
  provinceReducer,
  cityReducer,
  subdistrictReducer,
  kelurahanReducer,
  distributorRegionReducer,
  articleReducer,
  product_searchReducer,
  distributorSearchReducer,
  distributorSearchByLokasiReducer,
  distributorDetailReducer,
  loginReducer,
  pitcutereReducer,
  signUpReducer,
  logoutReducer,
  fotoKtpReducer,
  profileReducer,
  cartReducer,
  firstInstallReducer,
});

export default reducers;
