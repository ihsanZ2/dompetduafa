const initialState = {
  data: null,
  pitcure: [],
  isLoading: false,
};

export const pitcutereReducer = (state = initialState, action) => {
  const {payload} = action;
  //   console.log('action', action);
  switch (action.type) {
    case 'FETCH_UPLOAD_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_UPLOAD_SUCCESS':
      return {
        ...state,
        data: action.params,
        pitcure: payload,
        isLoading: false,
      };
    case 'FETCH_UPLOAD_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default pitcutereReducer;
