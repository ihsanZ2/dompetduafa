const initialStateFotoKtp = {
  data: null,
  foto_ktp: [],
  isLoading: false,
};

export const fotoKtpReducer = (state = initialStateFotoKtp, action) => {
  const {payload} = action;
  //   console.log('action', action);
  switch (action.type) {
    case 'FOTO_KTP_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FOTO_KTP_SUCCESS':
      return {
        ...state,
        data: action.params,
        foto_ktp: payload,
        isLoading: false,
      };
    case 'FOTO_KTP_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default fotoKtpReducer;
