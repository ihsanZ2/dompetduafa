const initialState = {
  data: null,
  product_search: [],
  isLoading: false,
};

export const product_searchReducer = (state = initialState, action) => {
  const {payload} = action;
  // console.log('action', action);
  switch (action.type) {
    case 'FETCH_PRODUCT_SEARCH_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_PRODUCT_SEARCH_SUCCESS':
      return {
        ...state,
        data: action.params,
        product_search: payload,
        isLoading: false,
      };
    case 'FETCH_PRODUCT_SEARCH_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'DELETE_PRODUCT_SEARCH':
      return {
        ...state,
        data: null,
        isLoading: false,
        product_search: [],
      };

    default:
      return state;
  }
};

export default product_searchReducer;
