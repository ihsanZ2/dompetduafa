const initialState = {
  subdistrict: [],
  isLoading: false,
};

export const subdistrictReducer = (state = initialState, action) => {
  const {payload} = action;
  switch (action.type) {
    case 'FETCH_SUBDISTRICT_REQUEST':
      return {
        ...state,
        isLoading: true,
      };

    case 'FETCH_SUBDISTRICT_SUCCESS':
      return {
        ...state,
        subdistrict: payload,
        isLoading: false,
      };
    case 'FETCH_SUBDISTRICT_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default subdistrictReducer;
