const initialState = {
  data: null,
  city: [],
  isLoading: false,
};

export const cityReducer = (state = initialState, action) => {
  const {payload} = action;
  //console.log('action', action);
  switch (action.type) {
    case 'FETCH_CITY_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_CITY_SUCCESS':
      return {
        ...state,
        data: action.params,
        city: payload,
        isLoading: false,
      };
    case 'FETCH_CITY_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default cityReducer;
