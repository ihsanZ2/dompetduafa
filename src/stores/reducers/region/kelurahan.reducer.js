const initialState = {
  kelurahan: [],
  isLoading: false,
};

export const kelurahanReducer = (state = initialState, action) => {
  const {payload} = action;
  switch (action.type) {
    case 'FETCH_KELURAHAN_REQUEST':
      return {
        ...state,
        isLoading: true,
      };

    case 'FETCH_KELURAHAN_SUCCESS':
      return {
        ...state,
        kelurahan: payload,
        isLoading: false,
      };
    case 'FETCH_KELURAHAN_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default kelurahanReducer;
