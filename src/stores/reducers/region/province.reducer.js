const initialState = {
  province: [],
  isLoading: false,
};

export const provinceReducer = (state = initialState, action) => {
  const {payload} = action;
  switch (action.type) {
    case 'FETCH_PROVINCE_REQUEST':
      return {
        ...state,
        isLoading: true,
      };

    case 'FETCH_PROVINCE_SUCCESS':
      return {
        ...state,
        province: payload,
        isLoading: false,
      };
    case 'FETCH_PROVINCE_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default provinceReducer;
