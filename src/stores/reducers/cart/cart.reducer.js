const initialState = {
  data: null,
  cart: [],
  isLoading: false,
};

export const cartReducer = (state = initialState, action) => {
  const {payload} = action;
  //   //console.log('action', action);
  switch (action.type) {
    case 'CART_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'CART_SUCCESS':
      return {
        ...state,
        data: action.params,
        cart: payload,
        isLoading: false,
      };
    case 'CART_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default cartReducer;
