const initialState = {
  data: null,
  profile: [],
  isLoading: false,
};

export const profileReducer = (state = initialState, action) => {
  const {payload} = action;
  //   console.log('action', action);
  switch (action.type) {
    case 'FETCH_PROFILE_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_PROFILE_SUCCESS':
      return {
        ...state,
        data: action.params,
        profile: payload,
        isLoading: false,
      };
    case 'FETCH_PROFILE_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    case 'FETCH_PROFILE_DELETE':
      return {
        ...state,
        profile: null,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default profileReducer;
