const initialState = {
  firstInstall: true,
  isLoading: false,
};

export const firstInstallReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FIRST_INSTALL':
      return {
        ...state,
        firstInstall: false,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default firstInstallReducer;
