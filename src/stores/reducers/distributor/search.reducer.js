const initialState = {
  data: null,
  distributor_search: [],
  isLoading: false,
};

export const distributorSearchReducer = (state = initialState, action) => {
  const {payload} = action;
  // console.log('action', action);
  switch (action.type) {
    case 'FETCH_DISTRIBUTOR_SEARCH_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_DISTRIBUTOR_SEARCH_SUCCESS':
      return {
        ...state,
        data: action.params,
        distributor_search: payload,
        isLoading: false,
      };
    case 'FETCH_DISTRIBUTOR_SEARCH_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default distributorSearchReducer;
