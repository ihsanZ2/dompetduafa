const initialState = {
  data: null,
  distributor_search_by_lokasi: [],
  isLoading: false,
};

export const distributorSearchByLokasiReducer = (
  state = initialState,
  action,
) => {
  const {payload} = action;
  //   console.log('action', action);
  switch (action.type) {
    case 'FETCH_DISTRIBUTOR_SEARCH_BY_LOKASI_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_DISTRIBUTOR_SEARCH_BY_LOKASI_SUCCESS':
      return {
        ...state,
        data: action.params,
        distributor_search_by_lokasi: payload,
        isLoading: false,
      };
    case 'FETCH_DISTRIBUTOR_SEARCH_BY_LOKASI_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default distributorSearchByLokasiReducer;
