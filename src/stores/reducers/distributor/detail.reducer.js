const initialState = {
  data: null,
  distributor_detail: [],
  isLoading: false,
};

export const distributorDetailReducer = (state = initialState, action) => {
  const {payload} = action;
  //   //console.log('action', action);
  switch (action.type) {
    case 'FETCH_DISTRIBUTOR_DETAIL_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_DISTRIBUTOR_DETAIL_SUCCESS':
      return {
        ...state,
        data: action.params,
        distributor_detail: payload,
        isLoading: false,
      };
    case 'FETCH_DISTRIBUTOR_DETAIL_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default distributorDetailReducer;
