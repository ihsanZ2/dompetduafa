const initialState = {
  distributorRegion: [],
  isLoading: false,
};

export const distributorRegionReducer = (state = initialState, action) => {
  const {payload} = action;
  switch (action.type) {
    case 'FETCH_DISTRIBUTOR_REGION_REQUEST':
      return {
        ...state,
        isLoading: true,
      };

    case 'FETCH_DISTRIBUTOR_REGION_SUCCESS':
      return {
        ...state,
        distributorRegion: payload,
        isLoading: false,
      };
    case 'FETCH_DISTRIBUTOR_REGION_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default distributorRegionReducer;
