const initialState = {
  data: null,
  singup: [],
  isLoading: false,
};

export const signUpReducer = (state = initialState, action) => {
  const {payload} = action;
  //console.log('action', action);
  switch (action.type) {
    case 'FETCH_SIGNUP_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_SIGNUP_SUCCESS':
      return {
        ...state,
        data: action.params,
        singup: payload,
        isLoading: false,
      };
    case 'FETCH_SIGNUP_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default signUpReducer;
