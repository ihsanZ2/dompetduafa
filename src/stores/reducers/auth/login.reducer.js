const initialState = {
  data: null,
  login: null,
  isLoading: false,
};

export const loginReducer = (state = initialState, action) => {
  const {payload} = action;
  //console.log('action', action);
  switch (action.type) {
    case 'FETCH_LOGIN_REQUEST':
      return {
        ...state,
        data: action.params,
        isLoading: true,
      };

    case 'FETCH_LOGIN_SUCCESS':
      return {
        ...state,
        data: action.params,
        login: payload,
        isLoading: false,
      };
    case 'FETCH_LOGIN_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'FETCH_LOGIN_DELETE':
      return {
        ...state,
        login: null,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default loginReducer;
