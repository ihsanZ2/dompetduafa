const initialState = {
  users: [],
  isLoading: false,
};

export const logoutReducer = (state = initialState, action) => {
  const {payload} = action;
  switch (action.type) {
    case 'FETCH_LOGOUT_REQUEST':
      return {
        ...state,
        isLoading: true,
      };

    case 'FETCH_LOGOUT_SUCCESS':
      return {
        ...state,
        users: payload,
        isLoading: false,
      };
    case 'FETCH_LOGOUT_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default logoutReducer;
