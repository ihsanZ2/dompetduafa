const initialState = {
  article: [],
  isLoading: false,
};

export const articleReducer = (state = initialState, action) => {
  const {payload} = action;
  switch (action.type) {
    case 'FETCH_ARTICLE_REQUEST':
      return {
        ...state,
        isLoading: true,
      };

    case 'FETCH_ARTICLE_SUCCESS':
      return {
        ...state,
        article: payload,
        isLoading: false,
      };
    case 'FETCH_ARTICLE_FAILED':
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default articleReducer;
