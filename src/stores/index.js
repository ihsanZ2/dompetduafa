import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import reducers from './reducers';
import {composeWithDevTools} from 'redux-devtools-extension';
import Reactotron from '../config/ReactrotronConfig';

// import {createLogger} from 'redux-logger';

// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/*
export const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['userReducer'],
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(thunk)),

  export const persistore = persistStore(store);
);
*/

const middleware = [thunk];
let composed = applyMiddleware(...middleware);
const createdEnhancer = Reactotron.createEnhancer();

if (process.env.NODE_ENV !== 'production') {
  // middleware.push(createLogger());
  composed = compose(
    composeWithDevTools(applyMiddleware(...middleware)),
    createdEnhancer,
  );
}

// Middleware: Redux Persist Config
export const persistConfig = {
  key: 'root',
  storage: AsyncStorage, // Storage Method (React Native)
  // whitelist: ['userReducer', 'provinceReducer'], // Whitelist (Save Specific Reducers)
  // blacklist: ['distributorRegionReducer'], // Blacklist (Don't Save Specific Reducers)
};

const persistedReducer = persistReducer(persistConfig, reducers);
export const store = createStore(persistedReducer, composed);
export const persistor = persistStore(store);
