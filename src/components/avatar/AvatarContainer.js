import React from 'react';
import {
  Image,
  View,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import ImagePicker, {ImageOrVideo} from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';
import InputSelectContainer from '../regionInput/InputSelectContainer';
import Colors from '../../theme/colors';
import Icon from '../../components/icon/Icon';

const CAMERA_ICON = 'md-camera';

const Avatar = props => {
  const valueKtp = useSelector(state => state.fotoKtpReducer.foto_ktp);
  const [uri, setUri] = React.useState(props.source?.uri || undefined);
  const [visible, setVisible] = React.useState(false);
  const close = () => {
    props.callbackClose(setVisible(false));
  };
  const open = () => {
    props.callbackOpen(setVisible(true));
  };

  const chooseImage = () => {
    ImagePicker.openPicker({
      mediaType: 'photo',
      compressImageMaxHeight: 720,
      compressImageMaxWidth: 720,
      forceJpg: true,
      includeBase64: true,
      cropping: true,
    })
      .then(image => {
        setUri(image.path);
        props.onChange?.(image);
      })
      .finally(close);
  };

  const openCamera = () => {
    ImagePicker.openCamera({
      mediaType: 'photo',
      compressImageMaxHeight: 1024,
      compressImageMaxWidth: 1024,
      forceJpg: true,
      multiple: true,
      includeBase64: true,
    })
      .then(image => {
        setUri(image.path);
        props.onChange?.(image);
      })
      .finally(close);
  };

  return (
    <>
      {props.type == 'avatar' ? (
        <TouchableOpacity onPress={open}>
          <Image
            style={styles.avatar}
            {...props}
            source={uri ? {uri} : props.source}
          />
          <View style={styles.whiteCircle}>
            <View style={styles.cameraButtonContainer}>
              <View style={styles.cameraButton}>
                <Icon
                  name={CAMERA_ICON}
                  size={16}
                  color={Colors.onPrimaryColor}
                />
              </View>
            </View>
          </View>
        </TouchableOpacity>
      ) : props.type == 'ktp' ? (
        <InputSelectContainer
          style={{marginBottom: 0}}
          placeholder="Foto KTP"
          value={props.value}
          onPress={open}
        />
      ) : null}

      <Modal
        isVisible={visible}
        onBackButtonPress={close}
        onBackdropPress={close}
        style={{justifyContent: 'flex-end', margin: 0}}>
        <SafeAreaView style={styles.options}>
          <Pressable style={styles.option} onPress={chooseImage}>
            {/* <ImageIcon /> */}
            <Text>Library </Text>
          </Pressable>
          <Pressable style={styles.option} onPress={openCamera}>
            {/* <CameraIcon /> */}
            <Text>Camera</Text>
          </Pressable>
        </SafeAreaView>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  avatar: {
    paddingTop: 20,
    height: 100,
    width: 100,
    borderRadius: 100,
    padding: 20,
  },

  options: {
    backgroundColor: 'white',
    flexDirection: 'row',
    height: '10%',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
  },
  option: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  whiteCircle: {
    marginTop: -18,
    justifyContent: 'center',
    alignSelf: 'center',
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: Colors.white,
  },
  cameraButtonContainer: {
    width: 34,
    height: 34,
    alignSelf: 'center',
    borderRadius: 17,
    backgroundColor: Colors.primaryColor,
    overflow: 'hidden',
  },
  cameraButton: {
    justifyContent: 'center',
    alignSelf: 'center',
    width: 34,
    height: 34,
  },
});

export default Avatar;
