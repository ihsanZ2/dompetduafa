/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {Component} from 'react';
import {
  I18nManager,
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
} from 'react-native';
import {SwipeRow} from 'react-native-swipe-list-view';
import {connect} from 'react-redux';

import Color from 'color';
import {convertToRupiah} from '../../utils/convertToRupiah.js';
// import utils
import getImgSource from '../../utils/getImgSource.js';

// import components
import Icon from '../icon/Icon';
import TouchableItem from '../TouchableItem';

// import colors
import Colors from '../../theme/colors';

// CardDcHorizontal Config
const imgHolder = require('../../assets/img/imgholder.png');

const isRTL = I18nManager.isRTL;
const IOS = Platform.OS === 'ios';
const MINUS_ICON = IOS ? 'ios-remove' : 'md-remove';
const PLUS_ICON = IOS ? 'ios-add' : 'md-add';
const DELETE_ICON = IOS ? 'ios-close' : 'md-close';
const RADIO_OFF_ICON = IOS ? 'ios-cart-outline' : 'cart';
const RADIO_ON_ICON = IOS ? 'trash' : 'trash';

// CardDcHorizontal Styles
const styles = StyleSheet.create({
  swipeRow: {
    borderRadius: 4,
    overflow: 'hidden',
  },
  cardBack: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin: 1,
    borderRadius: 4,
    backgroundColor: Color(Colors.error).alpha(0.12).string(),
  },
  deleteButtonContainer: {
    borderBottomRightRadius: 4,
    borderTopRightRadius: 4,
    width: 88,
    overflow: 'hidden',
  },
  deleteButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bg: {
    backgroundColor: Colors.background,
  },
  container: {
    flex: 1,
    margin: 4,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.08)',
    borderRadius: 4,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor: Colors.surface,
  },
  productImg: {
    width: 116,
    height: Dimensions.get('window').height * 0.17,
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4,
    resizeMode: 'cover',
  },
  productInfo: {
    flex: 1,
    paddingHorizontal: 10,
  },
  productDetails: {
    // flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'flex-start',
    // alignItems: 'flex-start',
    // padding: 12,
  },
  title: {
    flex: 1,
    fontWeight: '400',
    fontSize: 12,
    color: Colors.secondaryText,
    letterSpacing: 0.15,
    textAlign: 'left',
    marginBottom: 5,
  },
  priceContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  oldPrice: {
    fontSize: 15,
    fontWeight: '500',
    color: '#8e8e8e',
  },
  hr: {
    position: 'absolute',
    top: 10,
    width: '82%',
    height: 1,
    backgroundColor: '#8e8e8e',
  },
  price: {
    fontWeight: '700',
    fontSize: 18,
    color: Colors.primaryColor,
  },
  actionContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 4,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  quantity: {
    top: -1,
    paddingHorizontal: 20,
    fontSize: 18,
    color: Colors.black,
    textAlign: 'center',
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 8,
    width: 28,
    height: 28,
    borderRadius: 14,
    backgroundColor: Colors.secondaryColor,
  },
  newLabelContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    borderBottomRightRadius: 4,
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: Colors.primaryColor,
  },
  label: {
    fontSize: 12,
    color: Colors.onPrimaryColor,
  },
  radioIconContainer: {
    // flex:0,
    justifyContent: 'flex-end',
    flexDirection: 'row-reverse',
    paddingLeft: 10,
    alignSelf: 'center',
    marginRight: 16,
    width: 24,
    height: 24,
  },
});

// CardDcHorizontal DeleteButton
const DeleteButton = ({onPress}) => (
  <View style={styles.deleteButtonContainer}>
    <TouchableItem onPress={onPress} style={styles.deleteButton}>
      <Icon name={DELETE_ICON} size={26} color={Colors.error} />
    </TouchableItem>
  </View>
);

// CardDcHorizontal State
type State = {};

// CardDcHorizontal Props
type Props = {
  onPress: () => {},
  onPressRemove: () => void,
  onPressAdd: () => void,
  activeOpacity: number,
  imageUri: string,
  title: string,
  price: number,
  quantity: number,
  discountPercentage: number,
  swipeoutDisabled: boolean,
  swipeoutOnPressRemove: () => {},
  label: 'new',
};

// CardDcHorizontal
class ListProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: true,
    };
  }

  onPressAdd = () => {
    const {onPressAdd = () => {}} = this.props;
    onPressAdd();
  };

  onPressRemove = () => {
    const {onPressRemove = () => {}} = this.props;
    onPressRemove();
  };

  renderLabel = label => {
    if (label === 'new') {
      return (
        <View style={styles.newLabelContainer}>
          <Text style={styles.label}>NEW</Text>
        </View>
      );
    }

    return <View />;
  };

  redioActivePress = arg => {
    // console.log('activePressed',this.state.active)
    let cart = [];
    this.setState({
      active: !this.state.active,
    });

    if (this.props.item.id == arg.id) {
      // cart.push(arg.id)
      // if (this.state.active) {
      //   cart.push(arg.id)
      //   // this.props.onPress(this.props.item.id);
      // }
      // if(this.state.active==false){
      //   cart.pop()
      this.props.onPress(this.props.item.id);
      // }
      // console.log('cart',cart)
      // console.log(`${'cart',this.props.item.id}-${'arg',arg.id}-${'active',this.state.active}`)
    }
  };

  render() {
    const {
      activeOpacity,
      onPress,
      imageUri,
      title,
      swipeoutDisabled,
      swipeoutOnPressRemove,
      label,
      item,
    } = this.props;

    return (
      <View style={styles.container}>
        <SwipeRow
          disableLeftSwipe={isRTL ? true : swipeoutDisabled}
          disableRightSwipe={isRTL ? swipeoutDisabled : true}
          directionalDistanceChangeThreshold={16}
          rightOpenValue={isRTL ? 0 : -88}
          leftOpenValue={isRTL ? 88 : 0}
          style={styles.swipeRow}>
          <View style={styles.cardBack}>
            <DeleteButton onPress={swipeoutOnPressRemove} />
          </View>

          {/* FIX ME: extra View was added because of iOS ToucableOpacity bug */}
          <View style={styles.bg}>
            <TouchableItem
              activeOpacity={activeOpacity}
              onPress={() => this.redioActivePress(item)}
              useForeground>
              <View style={styles.innerContainer}>
                <Image
                  defaultSource={imgHolder}
                  source={getImgSource(imageUri)}
                  style={styles.productImg}
                />

                <View style={styles.productInfo}>
                  {/* <View style={styles.productDetails}> */}
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                      <Text numberOfLines={2} style={styles.title}>
                        {item.distributor_name}
                      </Text>
                      <Text numberOfLines={2} style={styles.title}>
                        {item.product_name}
                      </Text>
                      <Text numberOfLines={2} style={styles.title}>
                        {`Stok ${item.stock}`}
                      </Text>
                      <Text numberOfLines={2} style={styles.title}>
                        {convertToRupiah(item.price)}
                      </Text>
                      <Text numberOfLines={2} style={styles.title}>
                        {`${item.stock}${' '}${item.satuan} `}
                      </Text>
                      <Text numberOfLines={2} style={styles.title}>
                        {item.description}
                      </Text>
                    </View>
                    <View style={styles.radioIconContainer}>
                      {this.state.active ? (
                        <Icon
                          name={RADIO_OFF_ICON}
                          size={23}
                          color={Colors.primaryColor}
                        />
                      ) : (
                        <Icon
                          name={RADIO_ON_ICON}
                          size={21}
                          color={Colors.secondaryColor}
                        />
                      )}
                    </View>
                  </View>
                  {/* {discountPercentage ? (
                      <View style={styles.priceContainer}>
                        <Text style={styles.price}>
                          {`Rp${(
                            ((100 - discountPercentage) / 100) *
                            price
                          ).toFixed(2)}`}
                        </Text>
                        <View>
                          <Text style={styles.oldPrice}>
                            {`Rp${price.toFixed(2)}`}
                          </Text>
                          <View style={styles.hr} />
                        </View>
                      </View>
                    ) : (
                      <View style={styles.priceContainer}>
                        <Text style={styles.price}>
                          {`${convertToRupiah( price)}`}
                        </Text>
                      </View>
                    )} */}
                  {/* </View> */}

                  {/* <View style={styles.actionContainer}>
                    {quantity > 0 && (
                      <View style={styles.actions}>
                        <TouchableItem onPress={this.onPressRemove} borderless>
                          <View style={styles.iconContainer}>
                            <Icon
                              name={MINUS_ICON}
                              size={20}
                              color={Colors.onPrimaryColor}
                            />
                          </View>
                        </TouchableItem>

                        <Text style={styles.quantity}>{quantity}</Text>
                      </View>
                    )}
                    <TouchableItem onPress={this.onPressAdd} borderless>
                      <View style={styles.iconContainer}>
                        <Icon
                          name={PLUS_ICON}
                          size={20}
                          color={Colors.onPrimaryColor}
                        />
                      </View>
                    </TouchableItem>
                  </View> */}
                </View>

                {this.renderLabel(label)}
              </View>
            </TouchableItem>
          </View>
        </SwipeRow>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    product: state.product_searchReducer.product_search,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, null)(ListProduct);
