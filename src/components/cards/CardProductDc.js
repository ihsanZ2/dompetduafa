/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {Component} from 'react';
import {Image, Platform, StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';

// import utils
import getImgSource from '../../utils/getImgSource.js';
import {convertToRupiah} from '../../utils/convertToRupiah.js';
// import components
import {ButtonText} from '../text/CustomText';
import TouchableItem from '../TouchableItem';

// import colors, layout
import Colors from '../../theme/colors';
import Layout from '../../theme/layout';

// CardProductDc Config
const IOS = Platform.OS === 'ios';
const MINUS_ICON = IOS ? 'ios-remove' : 'md-remove';
const PLUS_ICON = IOS ? 'ios-add' : 'md-add';
const imgHolder = require('../../assets/img/imgholder.png');

// CardProductDc Styles
const styles = StyleSheet.create({
  container: {
    margin: 4,
    width: (Layout.SCREEN_WIDTH - 5 * 8) / 2,
  },
  borderContainer: {
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.08)',
    borderRadius: 5,
    backgroundColor: Colors.surface,
    overflow: 'hidden',
  },
  productImg: {
    width: '100%',
    height: 132,
    resizeMode: 'cover',
  },
  titleContainer: {
    paddingTop: 12,
    paddingHorizontal: 12,
    height: 52,
  },
  title: {
    fontWeight: '500',
    fontSize: 16,
    color: Colors.primaryText,
    letterSpacing: 0.15,
    textAlign: 'left',
  },
  productFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 12,
  },
  oldPriceContainer: {marginLeft: 5, paddingTop: 2},
  oldPrice: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#8e8e8e',
  },
  hr: {
    position: 'absolute',
    top: 13,
    width: '100%',
    height: 1,
    backgroundColor: '#8e8e8e',
  },
  price: {
    paddingTop: 4,
    fontWeight: '700',
    fontSize: 18,
    color: Colors.primaryColor,
  },
  actionContainer: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    backgroundColor: Colors.background,
    overflow: 'hidden',
  },
  action: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 42,
    overflow: 'hidden',
  },
  actionTitle: {
    color: Colors.onSecondaryColor,
    textAlign: 'center',
  },
  quantity: {
    top: -1,
    paddingHorizontal: 20,
    fontSize: 18,
    color: Colors.onSecondaryColor,
    textAlign: 'center',
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 4,
    width: 25,
    height: 25,
  },
  newLabelContainer: {
    position: 'absolute',
    top: 5,
    left: 5,
    borderRadius: 2,
    paddingVertical: 2,
    paddingHorizontal: 6,
    backgroundColor: Colors.primaryColor,
  },
  discountLabelContainer: {
    position: 'absolute',
    top: 5,
    right: 5,
    borderRadius: 2,
    paddingVertical: 2,
    paddingHorizontal: 6,
    backgroundColor: Colors.tertiaryColor,
  },
  label: {
    fontSize: 11,
    color: Colors.onPrimaryColor,
  },
});

// CardProductDc State
type State = {
  /**
   * Product in cart quantity
   */
  quantity: number,
};

// CardProductDc Props
type Props = {
  activeOpacity: number,
  /**
   * Product image uri
   */
  imageUri: string,
  /**
   * Product name
   */
  title: string,

  /**
   * Product price
   */
  price: number,

  /**
   * Product discount percentage
   */
  discountPercentage: number,

  /**
   * Handler to be called when the user taps on product card
   */
  onPress: () => void,
  label: 'new',
};

// CardProductDc
export default class CardProductDc extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 0,
    };
  }

  onPressAdd = () => {
    const {quantity} = this.state;
    const newQuantity = quantity + 1;

    this.setState({
      quantity: newQuantity,
    });
  };

  onPressRemove = () => {
    const {quantity} = this.state;
    const newQuantity = quantity - 1;

    this.setState({
      quantity: newQuantity,
    });
  };

  renderLabel = (label, discountPercentage) => {
    if (label === 'new') {
      return (
        <View style={styles.newLabelContainer}>
          <Text style={styles.label}>NEW</Text>
        </View>
      );
    }
    if (discountPercentage) {
      return (
        <View style={styles.discountLabelContainer}>
          <Text style={styles.label}>{`- ${discountPercentage}%`}</Text>
        </View>
      );
    }

    return <View />;
  };

  render() {
    const {
      activeOpacity,
      onPress,
      imageUri,
      title,
      price = 0,
      discountPercentage,
      label,
    } = this.props;

    const {quantity} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.borderContainer}>
          <TouchableItem
            activeOpacity={activeOpacity}
            onPress={onPress}
            borderless
            useForeground>
            <View>
              <Image
                defaultSource={imgHolder}
                source={getImgSource(imageUri)}
                style={styles.productImg}
              />

              {title !== undefined ? (
                <View style={styles.titleContainer}>
                  <Text numberOfLines={2} style={styles.title}>
                    {title}
                  </Text>
                </View>
              ) : null}

              {discountPercentage ? (
                <View style={styles.productFooter}>
                  <Text style={styles.price}>
                    {`Rp${((100 - discountPercentage) / 100) * price}`}
                  </Text>
                  <View style={styles.oldPriceContainer}>
                    <Text style={styles.oldPrice}>{`Rp${price}`}</Text>
                    <View style={styles.hr} />
                  </View>
                </View>
              ) : (
                <View style={styles.productFooter}>
                  <Text style={styles.price}>{`${convertToRupiah(
                    price,
                  )}`}</Text>
                </View>
              )}

              {this.renderLabel(label, discountPercentage)}
              {/* <View style={styles.action} /> */}
            </View>
          </TouchableItem>
        </View>
      </View>
    );
  }
}
