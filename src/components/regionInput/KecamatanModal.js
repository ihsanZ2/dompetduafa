import React, {Component, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Modal, FlatList, View, StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import {fetchDataKelurahan} from '../../stores/actions';

import RowItem from '../regionInput/RowItem';
import Colors from '../../theme/colors';
import SearchInput from '../regionInput/SearchInput';

const KecamatanModal = props => {
  const [kecamatanList, setKecamatanList] = useState([]);
  const [selectedKecamatan, setSelectedKecamatan] = useState(null);

  function onPressReset() {
    setSelectedKecamatan(null);
  }

  useEffect(() => {
    setKecamatanList(props.kecamatan.data);
  }, [props.kecamatan.data]);

  function onChangeSearch(search) {
    const {city, kecamatan} = props;
    let result = [];
    let citys = kecamatan.data;

    result = citys.filter(item =>
      item.subdistrict_name.toLowerCase().includes(search.toLowerCase()),
    );
    setKecamatanList(result);
  }

  function callbackSelectedItem(item) {
    const {onClose, callback} = props;
    const {data, status} = item;
    let selectedKecamatan = null;
    if (status) {
      selectedKecamatan = data;
    } else {
      if (data.id === selectedKecamatan.id) {
        selectedKecamatan = null;
      } else {
        selectedKecamatan = data;
      }
    }
    setSelectedKecamatan(selectedKecamatan);
    callback(selectedKecamatan);
    props.fetchDataKelurahan({subdistrict_id: selectedKecamatan.id});
    onClose();
  }

  const {visible, navigation, type, onClose} = props;
  return (
    <Modal visible={visible} onRequestClose={onClose} animationType="slide">
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: Colors.background,
        }}>
        <View style={styles.container}>
          <SearchInput
            placeholder="Cari Kota/Kabupaten"
            onChangeText={onChangeSearch}
          />
          <FlatList
            data={kecamatanList}
            extraData={selectedKecamatan}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <RowItem
                data={{id: item.id, name: item.subdistrict_name, ...item}}
                tick
                last={index === kecamatanList.length - 1}
                selected={selectedKecamatan}
                callback={callbackSelectedItem}
              />
            )}
            initialNumToRender={50}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

KecamatanModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  callback: PropTypes.func,
  type: PropTypes.oneOf(['city', 'kredit', 'los']),
};

KecamatanModal.defaultProps = {
  callback: () => null,
  type: 'city',
  kecamatan: [],
  visible: false,
};

const mapStateToProps = state => {
  //   let getCityStatus = state.location.kecamatanList;
  //   let kecamatan = state.session.kecamatan;
  return {
    kecamatan: state.subdistrictReducer.subdistrict,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataKelurahan: subdistrict_id =>
      dispatch(fetchDataKelurahan(subdistrict_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KecamatanModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
