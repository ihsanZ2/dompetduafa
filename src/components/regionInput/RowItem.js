import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../theme/colors';

const RowItem = props => {
  const [isSelected, setIsSelected] = useState(false);

  useEffect(() => {
    const {selected, data} = props;
    if (selected != null && selected.id == data.id) {
      setIsSelected(true);
    } else {
      setIsSelected(false);
    }
  }, [props, props.selected]);

  function onPressSelectItem() {
    const {callback, data} = props;
    callback({data, status: true});
  }

  const {data, last, tick} = props;
  return (
    <View>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.container}
        onPress={onPressSelectItem}>
        <View style={[styles.mainContainer]}>
          <Text style={styles.textResult}>{data.name}</Text>
          {isSelected && (
            <Icons name="check" light size={20} color={Colors.primaryColor} />
          )}
        </View>
      </TouchableOpacity>
      {!last && <View style={styles.border} />}
    </View>
  );
};

RowItem.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object]),
  last: PropTypes.bool,
  callback: PropTypes.func,
  selected: PropTypes.oneOfType([PropTypes.object]),
  tick: PropTypes.bool,
};

RowItem.defaultProps = {
  last: false,
  data: {
    id: 0,
    name: '',
  },
  callback: () => null,
  selected: null,
  tick: false,
};

export default RowItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    height: 57,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 20,
  },
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bullet: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.pinkish_grey,
  },
  activeBullet: {
    backgroundColor: Colors.primaryColor,
    borderWidth: 1,
    borderColor: Colors.primaryColor,
  },
  border: {
    height: 1,
    marginHorizontal: 10,
    backgroundColor: Colors.pinkish_grey,
  },
  textResult: {
    marginRight: 35,
    marginLeft: 5,
    fontSize: 14,
    color: Colors.black,
    fontWeight: '500',
    fontFamily: 'sans-serif',
  },
});
