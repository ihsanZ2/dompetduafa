import React from 'react';
import PropTypes from 'prop-types';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../../theme/colors';

const InputSelectContainer = props => {
  const {placeholder, value, onPress, disabled, style, label, iconName} = props;
  return (
    <View style={styles.containerMain}>
      {label.length > 0 && <Text style={styles.textLabel}>{label}</Text>}
      <TouchableOpacity
        disabled={disabled}
        activeOpacity={0.7}
        style={[
          styles.container,
          style,
          disabled && styles.disableContainerStyle,
        ]}
        onPress={onPress}>
        <View style={styles.textContainer}>
          {value && value.length > 0 ? (
            <Text style={styles.colorValue}>{value}</Text>
          ) : (
            <Text style={styles.placeholder}>{placeholder}</Text>
          )}
        </View>
        <View style={styles.iconContainer}>
          {!iconName ? (
            <Icon
              name="chevron-down"
              light
              color={Colors.pinkish_grey}
              size={15}
            />
          ) : (
            <Icon name={iconName} light color={Colors.pinkish_grey} size={15} />
          )}
        </View>
      </TouchableOpacity>
    </View>
  );
};

InputSelectContainer.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object]),
  label: PropTypes.string,
};

InputSelectContainer.defaultProps = {
  placeholder: 'placeholder',
  value: '',
  onPress: () => null,
  disabled: false,
  style: {},
  label: '',
};

export default InputSelectContainer;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: 40,
    flexDirection: 'row',
    borderRadius: 7,
    borderWidth: 0.8,
    borderColor: Colors.pinkish_grey,
    marginBottom: 10,
    paddingHorizontal: 5,
    alignItems: 'center',
    overflow: 'hidden',
  },
  containerMain: {
    marginVertical: 10,
  },
  textContainer: {
    flex: 1,
  },
  iconContainer: {
    width: 20,
    alignItems: 'flex-end',
  },
  disableContainerStyle: {
    backgroundColor: Colors.white_four,
  },
  placeholder: {
    fontFamily: 'sans-serif',
    fontWeight: '600',
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: 0,
    color: Colors.disabledText,
  },
  colorValue: {
    fontFamily: 'sans-serif',
    fontWeight: '600',
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: 0,
    color: Colors.black,
  },
});
