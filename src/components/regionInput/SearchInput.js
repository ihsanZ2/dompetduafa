import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {View, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import Colors from '../../theme/colors';

export default class SearchInput extends Component {
  // Prop type warnings
  static propTypes = {
    ...TextInput.propTypes,
  };

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor(props) {
    super(props);
    this.state = {
      keyword: '',
    };
    this.onPressIcon = this.onPressIcon.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  // componentDidMount (){
  //   const {onChangeText} = this.props;
  //   onChangeText('');
  // }

  onPressIcon() {
    const {onChangeText} = this.props;
    let {keyword} = this.state;
    if (keyword.length > 0) {
      this.searchInput.setNativeProps({text: ''});
      keyword = '';

      this.setState({keyword}, () => {
        if (onChangeText) {
          onChangeText('');
        }
      });
    }
  }

  onChangeText(keyword) {
    const {onChangeText} = this.props;

    this.setState({keyword}, () => {
      if (onChangeText) {
        onChangeText(keyword);
      }
    });
  }

  render() {
    const {keyword} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <TextInput
            {...this.props}
            ref={input => {
              this.searchInput = input;
            }}
            style={styles.searchTextInput}
            selectionColor={Colors.brownish_grey}
            defaultValue={keyword}
            placeholderTextColor={Colors.pinkish_grey}
            onChangeText={this.onChangeText}
            returnKeyType="search"
          />
          <TouchableOpacity activeOpacity={0.7} onPress={this.onPressIcon}>
            {keyword.length > 0 ? (
              <Icons
                name="times-circle"
                solid
                size={15}
                color={Colors.primaryColor}
              />
            ) : (
              <Icons
                name="search"
                light
                size={15}
                color={Colors.primaryColor}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: Colors.pinkish_grey,
    // borderBottomWidth: 0.7,
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: Colors.white,
  },
  searchContainer: {
    flexDirection: 'row',
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: Colors.pinkish_grey,
    paddingHorizontal: 10,
    height: 40,
    alignItems: 'center',
  },
  searchTextInput: {
    flex: 1,
  },
});
