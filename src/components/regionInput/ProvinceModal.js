import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  FlatList,
  View,
  StyleSheet,
  SafeAreaView,
  Text,
} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import {fetchDataCity} from '../../stores/actions';

// import ModalHeader from '../Components/Modalheader';
import RowItem from '../regionInput/RowItem';
import Colors from '../../theme/colors';
import SearchInput from '../regionInput/SearchInput';

const ProvinceModal = props => {
  let provinces = [...props.province];
  const [provinceList, setProvinceList] = useState(provinces);
  const [selectedProvince, setSelectedProvince] = useState(selectedProvince);

  function onPressReset() {
    setSelectedProvince(null);
  }

  function onChangeSearch(search) {
    const {province} = props;
    let result = [];

    if (search.length === 0) {
      result = [...province];
    } else {
      result = province?.filter(item =>
        item.province_name.toLowerCase().includes(search.toLowerCase()),
      );
    }
    setProvinceList(result);
  }

  function callbackSelectedItem(item) {
    const {data, status} = item;
    const {onClose, callback, fetchDataCity} = props;
    let selected = null;
    if (status) {
      selected = data;
    } else {
      if (data.id === selectedProvince.id) {
        selected = null;
      } else {
        selected = data;
      }
    }

    setSelectedProvince(selected);
    callback(selected);
    fetchDataCity({province_id: selected.id});
    onClose();
  }

  const {visible, navigation, onClose} = props;
  return (
    <Modal visible={visible} onRequestClose={onClose} animationType="slide">
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: Colors.background,
        }}
        forceInset={{bottom: 'always'}}>
        <View style={styles.container}>
          <SearchInput
            placeholder="Cari Provinsi"
            onChangeText={onChangeSearch}
          />
          <FlatList
            data={provinceList}
            extraData={selectedProvince}
            keyExtractor={item => item.id.toString()}
            renderItem={({item, index}) => (
              <RowItem
                data={{name: item.province_name, id: item.id, ...item}}
                tick
                last={index === provinceList.length - 1}
                selected={selectedProvince}
                callback={callbackSelectedItem}
              />
            )}
            initialNumToRender={50}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

ProvinceModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  callback: PropTypes.func,
  province: PropTypes.oneOfType([PropTypes.array]),
};

ProvinceModal.defaultProps = {
  callback: () => null,
  province: [],
  visible: false,
};

const mapStateToProps = state => {
  return {
    province: state.provinceReducer.province.data,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataCity: province_id => dispatch(fetchDataCity(province_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProvinceModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
