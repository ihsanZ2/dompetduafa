import React, {Component, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Modal, FlatList, View, StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

import RowItem from '../regionInput/RowItem';
import Colors from '../../theme/colors';
import SearchInput from '../regionInput/SearchInput';

const KelurahanModal = props => {
  const [kelurahanList, setKelurahanList] = useState([]);
  const [selectedKelurahan, setSelectedKelurahan] = useState(null);

  function onPressReset() {
    setSelectedKelurahan(null);
  }

  useEffect(() => {
    setKelurahanList(props.kelurahan.data);
  }, [props.kelurahan.data]);

  function onChangeSearch(search) {
    const {city, kelurahan} = props;
    let result = [];
    let citys = kelurahan.data;

    result = citys.filter(item =>
      item.kelurahan_name.toLowerCase().includes(search.toLowerCase()),
    );
    setKelurahanList(result);
  }

  function callbackSelectedItem(item) {
    const {onClose, callback} = props;
    const {data, status} = item;
    let selectedKelurahan = null;
    if (status) {
      selectedKelurahan = data;
    } else {
      if (data.id === selectedKelurahan.id) {
        selectedKelurahan = null;
      } else {
        selectedKelurahan = data;
      }
    }
    setSelectedKelurahan(selectedKelurahan);
    callback(selectedKelurahan);
    onClose();
  }

  const {visible, navigation, type, onClose} = props;
  return (
    <Modal visible={visible} onRequestClose={onClose} animationType="slide">
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: Colors.background,
        }}>
        <View style={styles.container}>
          <SearchInput placeholder="Kelurahan" onChangeText={onChangeSearch} />
          <FlatList
            data={kelurahanList}
            extraData={selectedKelurahan}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <RowItem
                data={{id: item.id, name: item.kelurahan_name, ...item}}
                tick
                last={index === kelurahanList.length - 1}
                selected={selectedKelurahan}
                callback={callbackSelectedItem}
              />
            )}
            initialNumToRender={50}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

KelurahanModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  callback: PropTypes.func,
  type: PropTypes.oneOf(['city', 'kredit', 'los']),
};

KelurahanModal.defaultProps = {
  callback: () => null,
  type: 'city',
  kelurahan: [],
  visible: false,
};

const mapStateToProps = state => {
  //   let getCityStatus = state.location.kelurahanList;
  //   let kelurahan = state.session.kelurahan;
  return {
    kelurahan: state.kelurahanReducer.kelurahan,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, null)(KelurahanModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
