import React, {Component, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Modal, FlatList, View, StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import {fetchDataSubdistrict} from '../../stores/actions';

import RowItem from '../regionInput/RowItem';
import Colors from '../../theme/colors';
import SearchInput from '../regionInput/SearchInput';

const CityModal = props => {
  const [cityList, setCityList] = useState([]);
  const [selectedCity, setSelectedCity] = useState(null);

  function onPressReset() {
    setSelectedCity(null);
  }

  useEffect(() => {
    setCityList(props.city.data);
  }, [props.city.data]);

  function onChangeSearch(search) {
    const {province, city} = props;
    let result = [];
    let citys = city.data;

    result = citys.filter(item =>
      item.city_name.toLowerCase().includes(search.toLowerCase()),
    );
    setCityList(result);
  }

  function callbackSelectedItem(item) {
    const {onClose, callback, fetchDataSubdistrict} = props;
    const {data, status} = item;
    let selectedCity = null;
    if (status) {
      selectedCity = data;
    } else {
      if (data.id === selectedCity.id) {
        selectedCity = null;
      } else {
        selectedCity = data;
      }
    }
    setSelectedCity(selectedCity);
    callback(selectedCity);
    fetchDataSubdistrict({city_id: selectedCity.id});
    onClose();
  }

  const {visible, navigation, type, onClose} = props;
  return (
    <Modal visible={visible} onRequestClose={onClose} animationType="slide">
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: Colors.background,
        }}>
        <View style={styles.container}>
          <SearchInput
            placeholder="Cari Kota/Kabupaten"
            onChangeText={onChangeSearch}
          />
          <FlatList
            data={cityList}
            extraData={selectedCity}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <RowItem
                data={{id: item.id, name: item.city_name, ...item}}
                tick
                last={index === cityList.length - 1}
                selected={selectedCity}
                callback={callbackSelectedItem}
              />
            )}
            initialNumToRender={50}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

CityModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  callback: PropTypes.func,
  type: PropTypes.oneOf(['province', 'kredit', 'los']),
};

CityModal.defaultProps = {
  callback: () => null,
  type: 'province',
  city: [],
  visible: false,
};

const mapStateToProps = state => {
  //   let getCityStatus = state.location.cityList;
  //   let city = state.session.city;
  return {
    city: state.cityReducer.city,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataSubdistrict: city_id => dispatch(fetchDataSubdistrict(city_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CityModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
