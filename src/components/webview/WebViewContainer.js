import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import {View} from 'react-native';
import {connect} from 'react-redux';

class WebViewContainer extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <WebView
          source={{uri: this.props.route.params.url}}
          onLoad={syntheticEvent => {
            // update component to be aware of loading status
            const {nativeEvent} = syntheticEvent;
            console.log('onStart', syntheticEvent);
            if (nativeEvent.url.includes('thankyou-page')) {
              // this.wv.stopLoading();
              this.props.navigation.pop(2);
              this.props.navigation.navigate('Orders');
            }
          }}>
          {console.log(this.props.route.params.url)}
        </WebView>
      </View>
    );
  }
}
export default connect(null, null)(WebViewContainer);
