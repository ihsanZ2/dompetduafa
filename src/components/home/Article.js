/* eslint-disable prettier/prettier */
// import utils
import getImgSource from '../../utils/getImgSource.js';
import { useNavigation } from '@react-navigation/native';

import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  I18nManager,
} from 'react-native';
import Color from 'color';
import Colors from '../../theme/colors';
import Swiper from 'react-native-swiper';
import {connect} from 'react-redux';
import TouchableItem from '../../components/TouchableItem';

const isRTL = I18nManager.isRTL;
const imgHolder = require('../../assets/img/imgholder.png');

function Article(props) {
  useEffect(() => {
    const {article} = props;
    if (article) {
      if (!article.isLoading) {
        if (article?.data) {
          setArticle(article.data);
        }
      }
    }
    return () => {};
  }, [props, props.article]);

  const [article, setArticle] = useState([]);
  const [index, setindex] = useState(0);
  const navigation = useNavigation();

  function renderItem(item, index ,props) {
    return (
      <View key={index} style={{marginHorizontal: 8, height: 225}}>
        <TouchableOpacity activeOpacity={0.9} onPress={()=>navigation.navigate('WebViewArticle',{url:item.url})}>
          <ImageBackground
            defaultSource={imgHolder}
            source={getImgSource(item.image)}
            imageStyle={styles.cardImg}
            style={styles.card}
          />
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View style={styles.swiperContainer}>
      {article.length > 0 ?
    <Swiper
    loop={true}
    autoplay={true}
    autoplayTimeout={5}
    removeClippedSubviews={false}
    paginationStyle={styles.paginationStyle}
    activeDotStyle={styles.activeDot}
    dotStyle={styles.dot}
    snapToEnd={false}
    index={isRTL ? article.length - 1 : 0}>
    {article.map((item, i) => renderItem(item, i, props))}
  </Swiper>
      : <View>
          <ImageBackground
      key={index}
      defaultSource={imgHolder}
      imageStyle={styles.cardImg}
      style={styles.card}>
      <View style={styles.cardOverlay} />
    </ImageBackground>
      </View>}

    </View>
  );
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  container: {
    flex: 1,
  },
  categoriesContainer: {
    paddingBottom: 16,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 16,
    paddingHorizontal: 16,
    paddingBottom: 12,
  },
  titleText: {
    fontWeight: '700',
  },
  viewAllText: {
    color: Colors.primaryColor,
  },
  categoriesList: {
    paddingTop: 4,
    paddingRight: 16,
    paddingLeft: 8,
  },
  cardImg: {borderRadius: 4, width: '100%', height: 225},

  swipper: {
    width: '20%',
    height: '20%',
  },
  card: {
    width: '100%',
    height: 225,
    resizeMode: 'cover',
  },
  cardOverlay: {
    flex: 1,
    borderRadius: 4,
    backgroundColor: Color(Colors.overlayColor).alpha(0.2),
    overflow: 'hidden',
    marginHorizontal:8,
  },
  cardContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cardTitle: {
    padding: 12,
    fontWeight: '500',
    fontSize: 16,
    color: Colors.white,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  productsList: {
    paddingBottom: 16,
    // spacing = paddingHorizontal + ActionProductCard margin = 12 + 4 = 16
    paddingHorizontal: 12,
  },
  popularProductsList: {
    // spacing = paddingHorizontal + ActionProductCardHorizontal margin = 12 + 4 = 16
    paddingHorizontal: 12,
    paddingBottom: 16,
  },
  swiperContainer: {
    width: '100%',
    height: 228,
  },
  paginationStyle: {
    bottom: 12,
    transform: [{scaleX: isRTL ? -1 : 1}],
  },
  dot: {backgroundColor: Colors.background},
  activeDot: {backgroundColor: Colors.primaryColor},
  slideImg: {
    width: '100%',
    height: 228,
    resizeMode: 'cover',
  },
});

const mapStateToProps = (state) => {
  return {
    article: state.articleReducer.article,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Article);
