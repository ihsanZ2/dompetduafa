import {Text, ToastAndroid, View} from 'react-native';

import React from 'react';

function Toast(message) {
  if (message) {
    return ToastAndroid.show(message, ToastAndroid.SHORT);
  }
}

export default Toast;
