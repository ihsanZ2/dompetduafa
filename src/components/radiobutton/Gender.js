import React, {Component} from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import color from '../../theme/colors';

export default class RadioButton extends Component {
  state = {
    value: null,
  };

  render() {
    const {value} = this.state;
    return (
      <View style={{flexDirection: 'row'}}>
        {this.props.data.map(res => {
          return (
            <View key={res.key} style={styles.container}>
              <TouchableOpacity
                style={styles.radioCircle}
                onPress={() => {
                  this.setState(
                    {
                      value: res.key,
                    },
                    this.props.callback(res.key),
                  );
                }}>
                {value === res.key && <View style={styles.selectedRb} />}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState(
                    {
                      value: res.key,
                    },
                    this.props.callback(res.key),
                  );
                }}>
                <Text
                  style={
                    value !== res.key ? styles.radioText : styles.radioTextlBack
                  }>
                  {res.text}
                </Text>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    alignItems: 'center',
    flexDirection: 'row',
    flex: 0.4,
    // justifyContent: 'space-between',
  },
  radioText: {
    marginRight: 35,
    marginLeft: 5,
    fontSize: 14,
    color: color.pinkish_grey,
    fontWeight: '400',
    fontFamily: 'sans-serif',
  },
  radioTextlBack: {
    marginRight: 35,
    marginLeft: 5,
    fontSize: 14,
    color: color.black,
    fontWeight: '400',
    fontFamily: 'sans-serif',
  },
  radioCircle: {
    height: 15,
    width: 15,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: '#00b970',
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedRb: {
    width: 5,
    height: 5,
    borderRadius: 50,
    backgroundColor: '#00b970',
  },
  result: {
    marginTop: 20,
    color: 'white',
    fontWeight: '600',
    backgroundColor: '#F3FBFE',
  },
});
