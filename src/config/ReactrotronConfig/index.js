import {reactotronRedux} from 'reactotron-redux';
import {isEmulator} from 'react-native-device-info';
import Reactotron, {networking} from 'reactotron-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const name = `unmute App ${isEmulator() ? 'Emulator' : 'Real Device'}`;
const host = isEmulator() ? '127.0.0.1' : 'localhost';
const configOptions = {name, host};

const reactotron = Reactotron.configure(configOptions)
  .use(reactotronRedux())
  .setAsyncStorageHandler(AsyncStorage)
  .use(networking())
  .useReactNative()
  .connect();

// patch console.log to send log to reactotron
const oldConsoleLog = console.log;
console.log = (...args) => {
  oldConsoleLog(...args);

  Reactotron.display({
    name: 'CONSOLE.LOG',
    value: args,
    preview: args.length > 0 && typeof args[0] === 'string' ? args[0] : null,
  });
};

// clear log on start
Reactotron.clear();

export default reactotron;
