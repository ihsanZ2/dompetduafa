function convertToRupiah(angka) {
  if (angka) {
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for (var i = 0; i < angkarev.length; i++) {
      if (i % 3 == 0) {
        rupiah += angkarev.substr(i, 3) + '.';
      }
    }
    return (
      'Rp. ' +
      rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('')
    );
  } else {
    return 'Rp. ' + 0;
  }
}

function convertToAngka(rupiah) {
  return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
}

function dateFormat(arg) {
  let temp = arg.split('');
  let remove = temp.splice(2, 0, '-');
  let dates = temp.join('');
  return dates;
}

export {convertToAngka, convertToRupiah, dateFormat};
