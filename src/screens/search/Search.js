/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {Component} from 'react';
import {
  FlatList,
  I18nManager,
  ImageBackground,
  Keyboard,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import Color from 'color';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// import utils
import getImgSource from '../../utils/getImgSource.js';

// import components
import TouchableItem from '../../components/TouchableItem';
import {Heading6} from '../../components/text/CustomText';

// import colors
import Colors from '../../theme/colors';
import {connect} from 'react-redux';

import {fetchDataDistributorSearchByLokasi} from '../../stores/actions/distributor/searchByLokasi.action.js';
import {fetchDataDistributorDetail} from '../../stores/actions/index.js';

// Search Configƒ
const isRTL = I18nManager.isRTL;
const SEARCH_ICON = 'magnify';
const imgHolder = require('../../assets/img/imgholder.png');

// Search Styles
const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  container: {
    flex: 1,
  },
  titleContainer: {
    paddingHorizontal: 16,
  },
  titleText: {
    paddingTop: 16,
    paddingBottom: 8,
    fontWeight: '700',
    textAlign: 'left',
  },
  inputContainer: {
    marginHorizontal: 16,
    paddingBottom: 10,
  },
  textInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.12)',
    paddingLeft: 8,
    paddingRight: 51,
    height: 46,
    fontSize: 16,
    textAlignVertical: 'center',
    textAlign: isRTL ? 'right' : 'left',
  },
  searchButtonContainer: {
    position: 'absolute',
    top: 4,
    right: 4,
    borderRadius: 4,
    backgroundColor: Colors.primaryColor,
    overflow: 'hidden',
  },
  searchButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 38,
    height: 38,
  },
  categoriesList: {
    paddingBottom: 10,
  },
  cardImg: {borderRadius: 4},
  card: {
    marginVertical: 6,
    marginHorizontal: 16,
    height: 100,
    resizeMode: 'cover',
  },
  cardOverlay: {
    flex: 1,
    borderRadius: 4,
    backgroundColor: Color(Colors.overlayColor).alpha(0.2),
    overflow: 'hidden',
  },
  cardContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cardTitle: {
    padding: 16,
    fontWeight: '700',
    fontSize: 18,
    color: Colors.white,
    textShadowColor: 'rgba(0, 0, 0, 0.88)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
});

// Search
class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      categories: [
        {
          key: 1,
          imageUri: require('../../assets/img/pizza_3.jpg'),
          name: 'Pizza',
        },
        {
          key: 2,
          imageUri: require('../../assets/img/meat_1.jpg'),
          name: 'Grill',
        },
        {
          key: 3,
          imageUri: require('../../assets/img/spaghetti_2.jpg'),
          name: 'Pasta',
        },
        {
          key: 4,
          imageUri: require('../../assets/img/soup_1.jpg'),
          name: 'Soups',
        },
        {
          key: 5,
          imageUri: require('../../assets/img/salad_1.jpg'),
          name: 'Salads',
        },
        {
          key: 6,
          imageUri: require('../../assets/img/cake_2.jpg'),
          name: 'Dessert',
        },
      ],
    };
  }

  navigateTo = screen => () => {
    const {navigation} = this.props;

    Keyboard.dismiss();

    navigation.navigate(screen);
  };

  keyExtractor = (item, index) => index.toString();

  renderCategoryItem = ({item, index}) => (
    <ImageBackground
      key={index}
      defaultSource={imgHolder}
      source={getImgSource(item.image || '')}
      imageStyle={styles.cardImg}
      style={styles.card}>
      <View style={styles.cardOverlay}>
        <TouchableItem
          onPress={()=>this.props.fetchDataDistributorSearchByLokasi({
            distributor_name: item.city_name,
          })}
          style={styles.cardContainer}
          // borderless
        >
          <Text style={styles.cardTitle}>{item.city_name}</Text>
        </TouchableItem>
      </View>
    </ImageBackground>
  );

  handleTextInput = text => {
    this.setState({search: text});
  };

  onSearch = () => {
    const {search} = this.state;
    const {fetchDataDistributorSearchByLokasi} = this.props;
    fetchDataDistributorSearchByLokasi({
      distributor_name: search,
    });
    this.setState({search: ''});
  };

  render() {
    const {categories} = this.state;
    const {searchDistributor} = this.props;

    return (
      <SafeAreaView style={styles.screenContainer}>
        <StatusBar
          backgroundColor={Colors.statusBarColor}
          barStyle="dark-content"
        />

        <View style={styles.titleContainer}>
          <Heading6 style={styles.titleText}>Search</Heading6>
        </View>

        <View style={styles.inputContainer}>
          <TextInput
            ref={el => {
              this.username = el;
            }}
            placeholder="Cari Berdasarkan Cabang DC"
            returnKeyType="search"
            maxLength={50}
            style={styles.textInput}
            value={this.state.search}
            onChangeText={this.handleTextInput}
            onSubmitEditing={this.onSearch}
          />
          <View style={styles.searchButtonContainer}>
            <TouchableItem
              onPress={this.onSearch}
              // borderless
            >
              <View style={styles.searchButton}>
                <Icon
                  name={SEARCH_ICON}
                  size={23}
                  color={Colors.onPrimaryColor}
                />
              </View>
            </TouchableItem>
          </View>
        </View>

        <View style={styles.container}>
          <FlatList
            data={
              searchDistributor?.data && searchDistributor?.data.length > 0
                ? searchDistributor?.data
                : categories
            }
            showsHorizontalScrollIndicator={false}
            alwaysBounceHorizontal={false}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderCategoryItem}
            contentContainerStyle={styles.categoriesList}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    searchLokasiDistributor:
      state.distributorSearchByLokasiReducer.distributor_search_by_lokasi,
    searchDistributor: state.distributorRegionReducer.distributorRegion,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataDistributorSearchByLokasi: product_search =>
      dispatch(fetchDataDistributorSearchByLokasi(product_search)),
    fetchDataDistributorDetail: distributor =>
      dispatch(fetchDataDistributorDetail(distributor)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
