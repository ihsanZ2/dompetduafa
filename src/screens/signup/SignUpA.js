/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {connect} from 'react-redux';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

// import components
import Button from '../../components/buttons/Button';
import UnderlinePasswordInput from '../../components/textinputs/UnderlinePasswordInput';
import UnderlineTextInput from '../../components/textinputs/UnderlineTextInput';
import Avatar from '../../components/avatar/AvatarContainer';
import ProvinceModal from '../../components/regionInput/ProvinceModal';
import CityModal from '../../components/regionInput/CityModal';
import KecamatanModal from '../../components/regionInput/KecamatanModal';
import KelurahanModal from '../../components/regionInput/KelurahanModal';
import InputSelectContainer from '../../components/regionInput/InputSelectContainer';
import Gender from '../../components/radiobutton/Gender';
import Toast from '../../components/toast/Toast';

// import colors, layout
import Colors from '../../theme/colors';
import Layout from '../../theme/layout';

import {fetchUpload, fetchKtpUpload, fetchSignUp} from '../../stores/actions';

// SignUpA Config
const PLACEHOLDER_TEXT_COLOR = 'rgba(0, 0, 0, 0.4)';
const INPUT_TEXT_COLOR = Colors.black;
const INPUT_BORDER_COLOR = 'rgba(0, 0, 0, 0.2)';
const INPUT_FOCUSED_BORDER_COLOR = '#000';

// SignUpA Styles
const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  contentContainerStyle: {
    flex: 1,
  },
  content: {
    flex: 1,
    justifyContent: 'space-between',
  },
  form: {
    paddingHorizontal: Layout.LARGE_PADDING,
  },
  inputContainer: {marginBottom: 7},
  vSpacer: {
    height: 15,
  },
  buttonContainer: {
    paddingVertical: 23,
  },
  buttonsGroup: {
    paddingTop: 23,
  },
  separator: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    width: 64,
    height: 1,
    backgroundColor: INPUT_BORDER_COLOR,
  },
  orText: {
    top: -2,
    paddingHorizontal: 8,
    color: PLACEHOLDER_TEXT_COLOR,
  },
  footer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    width: '100%',
  },
  termsContainer: {
    flexDirection: 'row',
  },
  footerText: {
    fontWeight: '300',
    fontSize: 13,
    color: Colors.primaryText,
  },
  footerLink: {
    fontWeight: '400',
    textDecorationLine: 'underline',
  },
  placeholder: {
    fontFamily: 'sans-serif',
    fontWeight: '600',
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: 0,
    color: Colors.disabledText,
    marginRight: 10,
  },
  jekel: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 0,
  },
});

// SignUpA
class SignUpA extends Component {
  constructor(props) {
    super(props);
    this.child = React.createRef();
    this.state = {
      email: '',
      emailFocused: false,
      phone: null,
      nik: null,
      phoneFocused: false,
      password: '',
      passwordFocused: false,
      secureTextEntry: true,
      showProvinsi: false,
      showCity: false,
      showKecamatan: false,
      showKelurahan: false,
      provinsi: null,
      kota: null,
      kecamatan: null,
      kelurahan: null,
      gender: null,
      profile_type: null,
      full_name: '',
      birth_date: null,
      picture: null,
      photo_ktp: null,
      address: null,
      isDatePickerVisible: false,
    };
  }

  genderData = [
    {
      key: 'pria',
      text: 'Pria',
    },
    {
      key: 'wanita',
      text: 'Wanita',
    },
  ];
  profileData = [
    {
      key: 'Komunitas',
      text: 'Komunitas',
    },
    {
      key: 'Individu',
      text: 'Individu',
    },
  ];

  emailChange = text => {
    this.setState({
      email: text,
    });
  };

  emailFocus = () => {
    this.setState({
      emailFocused: true,
      phoneFocused: false,
      passwordFocused: false,
    });
  };

  phoneChange = text => {
    this.setState({
      phone: text,
    });
  };

  fullNameChange = text => {
    this.setState({
      full_name: text,
    });
  };

  phoneFocus = () => {
    this.setState({
      phoneFocused: true,
      emailFocused: false,
      passwordFocused: false,
    });
  };

  passwordChange = text => {
    this.setState({
      password: text,
    });
  };

  nikChange = text => {
    this.setState({
      nik: text,
    });
  };

  alamatChange = text => {
    this.setState({
      address: text,
    });
  };

  passwordFocus = () => {
    this.setState({
      passwordFocused: true,
      emailFocused: false,
      phoneFocused: false,
    });
  };

  onTogglePress = () => {
    const {secureTextEntry} = this.state;
    this.setState({
      secureTextEntry: !secureTextEntry,
    });
  };

  navigateTo = screen => () => {
    const {navigation} = this.props;
    navigation.navigate(screen);
  };

  createAccount = () => {
    if (!this.props.pitcure) {
      Toast('Foto Profile diperlukan');
      console.log('this.props.picture', this.props.pitcure);
      return;
    }
    if (this.state.phone == null) {
      Toast('User Name diperlukan');
      return;
    }
    if (this.state.full_name.length == 0) {
      Toast('Nama diperlukan');
      return;
    }
    if (this.state.email.length === 0) {
      Toast('Email diperlukan');
      return;
    }
    if (this.state.password.length == 0) {
      Toast('Password diperlukan');
      return;
    }
    if (this.state.gender == null) {
      Toast('Jenis Kelamin diperlukan');
      return;
    }
    if (this.state.profile_type == null) {
      Toast('Type Profile diperlukan');
      return;
    }
    if (this.state.nik == null) {
      Toast('NIK diperlukan');
      return;
    }
    if (this.props.photoKtp == null) {
      Toast('Foto Ktp diperlukan');
      return;
    }
    if (this.state.birth_date == null) {
      Toast('Tanggal Lahir diperlukan');
      return;
    }

    if (this.state.provinsi == null) {
      Toast('Provinsi diperlukan');
      return;
    }
    if (this.state.kota == null) {
      Toast('Kabupaten/Kota diperlukan');
      return;
    }
    if (this.state.kecamatan == null) {
      Toast('Kecamatan diperlukan');
      return;
    }
    if (this.state.kelurahan == null) {
      Toast('Kelurahan diperlukan');
      return;
    }
    if (this.state.address.length === 0) {
      Toast('Alamat diperlukan');
      return;
    }
    console.log({
      // username: this.state.phone,
      // email: this.state.email,
      // password: this.state.password,
      // profile_type: this.state.profile_type,
      // full_name: this.state.full_name,
      // gender: this.state.gender,
      // birth_date: '1991-10-10',
      // nik: '0987654321234567',
      picture: this.props.pitcure,
      // photo_ktp: 'profile_1637943905.jpg',
      // province_id: this.state.provinsi.id,
      // city_id: this.state.kota.id,
      // subdistrict_id: this.state.kecamatan.id,
      // kelurahan_id: this.state.kelurahan.id,
      // address: this.state.address,
    });
    let param = {
      username: this.state.phone,
      email: this.state.email,
      password: this.state.password,
      profile_type: this.state.profile_type,
      full_name: this.state.full_name,
      gender: this.state.gender,
      birth_date: this.state.birth_date,
      nik: this.state.nik,
      picture: this.props.pitcure,
      photo_ktp: this.props.photoKtp,
      province_id: this.state.provinsi.id,
      city_id: this.state.kota.id,
      subdistrict_id: this.state.kecamatan.id,
      kelurahan_id: this.state.kelurahan.id,
      address: this.state.address,
    };
    this.props.fetchSignUp(param);
  };

  // toLoginPage = () => {
  //   const {navigation} = this.props;
  //   navigation.navigate('Home');
  //   navigation.navigate('SignIn');
  // };

  focusOn = nextFiled => () => {
    if (nextFiled) {
      nextFiled.focus();
    }
  };

  onAvatarChange = image => {
    // console.log(image.data);
    // upload image to server here
    if (image && image.data) {
      this.props.fetchUpload({
        image: `data:image/jpeg;base64,${image.data}`,
      });
    }
    return;
  };
  onKTPChange = imageRes => {
    // console.log('KTP CHANGE', imageRes.modificationDate);
    // this.setState({
    //   photo_ktp: imageRes,
    // });
    if (imageRes && imageRes.data) {
      this.props.fetchKtpUpload({
        image: `data:image/jpeg;base64,${imageRes.data}`,
      });
    }
    return;
  };

  callbackImageUpload = imageRes => {
    console.log('imageRes', imageRes);
    this.setState({
      pitcure: imageRes,
    });
  };

  provinceFocus = () => {
    this.setState({
      ...this.state,
      showProvinsi: true,
    });
  };

  cityFocus = () => {
    this.setState({
      ...this.state,
      showCity: true,
    });
  };

  kecamatanFocus = () => {
    this.setState({
      ...this.state,
      showKecamatan: true,
    });
  };

  kelurahanFocus = () => {
    this.setState({
      ...this.state,
      showKelurahan: true,
    });
  };

  onCloseProvinsi = () => {
    this.setState({showProvinsi: false});
  };
  onCloseCity = () => {
    this.setState({showCity: false});
  };

  onCloseKecamatan = () => {
    this.setState({showKecamatan: false});
  };

  onCloseKelurahan = () => {
    this.setState({showKelurahan: false});
  };

  callbackJekel = jekel => {
    this.setState({...this.state, gender: jekel});
  };

  callbacProfileType = type => {
    this.setState({...this.state, profile_type: type});
  };

  callbackProvinsi = newProvinsi => {
    const {provinsi} = this.state;
    let vprovinsi, vkota, vkecamatan, vkelurahan;
    if (provinsi != null) {
      if (newProvinsi.id !== provinsi.id) {
        vprovinsi = newProvinsi;
        vkota = null;
        vkecamatan = null;
        vkelurahan = null;
        this.setState({
          provinsi: vprovinsi,
          kota: vkota,
          kecamatan: vkecamatan,
          kelurahan: vkelurahan,
        });
      }
    } else {
      this.setState({
        provinsi: newProvinsi,
      });
    }
  };

  callbackKota = newKota => {
    const {kota} = this.state;
    let vkota, vkecamatan, vkelurahan;
    if (kota != null) {
      if (newKota.id != kota.id) {
        vkota = newKota;
        this.setState({
          kota: vkota,
        });
      }
    } else {
      this.setState({
        kota: newKota,
      });
    }
  };

  callbackKecamatan = newKec => {
    const {kota} = this.state;
    let vkecamatan, vkelurahan;
    if (kota != null) {
      if (newKec.id != kota.id) {
        vkecamatan = newKec;
        this.setState({
          kecamatan: vkecamatan,
          kelurahan: null,
        });
      }
    } else {
      this.setState({
        kecamatan: newKec,
      });
    }
  };

  callbackKelurahan = newKlurahan => {
    this.setState({
      kelurahan: newKlurahan,
    });
  };

  showDatePicker = () => {
    this.setState({...this.state, isDatePickerVisible: true});
  };

  hideDatePicker = () => {
    this.setState({...this.state, isDatePickerVisible: false});
  };
  handleConfirm = date => {
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let fullYear = date.getFullYear();
    let dates = `${fullYear}-${month}-${day}`;
    this.setState({...this.state, birth_date: dates});
    console.warn('A date has been picked: ', dates);
    this.hideDatePicker();
  };

  render() {
    const {
      password,
      secureTextEntry,
      showProvinsi,
      showCity,
      provinsi,
      kota,
      kecamatan,
      kelurahan,
      birth_date,
      photo_ktp,
    } = this.state;

    return (
      <SafeAreaView style={styles.screenContainer}>
        <StatusBar
          backgroundColor={Colors.statusBarColor}
          barStyle="dark-content"
        />
        <ScrollView>
          <KeyboardAwareScrollView
            contentContainerStyle={styles.contentContainerStyle}>
            <View style={styles.content}>
              <View />
              <View style={styles.form}>
                <View
                  style={{
                    alignSelf: 'center',
                    paddingTop: 15,
                    paddingBottom: 20,
                  }}>
                  <Avatar
                    type={'avatar'}
                    callbackOpen={() => {}}
                    callbackClose={() => {}}
                    onChange={this.onAvatarChange}
                    source={require('../../assets/img/profile_1.jpeg')}
                  />
                </View>
                <ProvinceModal
                  visible={showProvinsi}
                  onClose={this.onCloseProvinsi}
                  defaultValue={provinsi}
                  callback={this.callbackProvinsi}
                />
                <CityModal
                  visible={showCity}
                  onClose={this.onCloseCity}
                  defaultValue={kota}
                  province={provinsi}
                  callback={this.callbackKota}
                />
                <KecamatanModal
                  visible={this.state.showKecamatan}
                  onClose={this.onCloseKecamatan}
                  defaultValue={kecamatan}
                  city={kota}
                  callback={this.callbackKecamatan}
                />
                <KelurahanModal
                  visible={this.state.showKelurahan}
                  onClose={this.onCloseKelurahan}
                  defaultValue={kelurahan}
                  kecamatan={kecamatan}
                  callback={this.callbackKelurahan}
                />

                <DateTimePickerModal
                  isVisible={this.state.isDatePickerVisible}
                  mode="date"
                  onConfirm={this.handleConfirm}
                  onCancel={this.hideDatePicker}
                />

                <UnderlineTextInput
                  onRef={r => {
                    this.phone = r;
                  }}
                  onChangeText={this.phoneChange}
                  blurOnSubmit={false}
                  keyboardType="phone-pad"
                  placeholder="User Name"
                  placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                  inputTextColor={INPUT_TEXT_COLOR}
                  borderColor={INPUT_BORDER_COLOR}
                  focusedBorderColor={INPUT_FOCUSED_BORDER_COLOR}
                  inputContainerStyle={styles.inputContainer}
                />

                <UnderlineTextInput
                  onRef={r => {
                    this.full_name = r;
                  }}
                  onChangeText={this.fullNameChange}
                  returnKeyType="done"
                  blurOnSubmit={false}
                  keyboardType="default"
                  placeholder="Nama Lengkap"
                  placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                  inputTextColor={INPUT_TEXT_COLOR}
                  borderColor={INPUT_BORDER_COLOR}
                  focusedBorderColor={INPUT_FOCUSED_BORDER_COLOR}
                  inputContainerStyle={styles.inputContainer}
                />

                <UnderlineTextInput
                  onRef={r => {
                    this.email = r;
                  }}
                  onChangeText={this.emailChange}
                  returnKeyType="done"
                  blurOnSubmit={false}
                  keyboardType="email-address"
                  placeholder="E-mail"
                  placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                  inputTextColor={INPUT_TEXT_COLOR}
                  borderColor={INPUT_BORDER_COLOR}
                  focusedBorderColor={INPUT_FOCUSED_BORDER_COLOR}
                  inputContainerStyle={styles.inputContainer}
                />

                <UnderlinePasswordInput
                  onRef={r => {
                    this.password = r;
                  }}
                  onChangeText={this.passwordChange}
                  returnKeyType="done"
                  placeholder="Password"
                  placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                  secureTextEntry={secureTextEntry}
                  borderColor={INPUT_BORDER_COLOR}
                  focusedBorderColor={INPUT_FOCUSED_BORDER_COLOR}
                  toggleVisible={password.length > 0}
                  toggleText={secureTextEntry ? 'Show' : 'Hide'}
                  onTogglePress={this.onTogglePress}
                />

                <View style={styles.jekel}>
                  <View style={{width: '32%'}}>
                    <Text style={styles.placeholder}>{'Jenis Kelamin'}</Text>
                  </View>
                  <Text style={styles.placeholder}>{':'}</Text>
                  <Gender
                    data={this.genderData}
                    callback={this.callbackJekel}
                  />
                </View>

                <View style={styles.jekel}>
                  <View style={{width: '32%'}}>
                    <Text style={styles.placeholder}>{'Type Profil'}</Text>
                  </View>
                  <Text style={styles.placeholder}>{':'}</Text>
                  <Gender
                    data={this.profileData}
                    callback={this.callbacProfileType}
                  />
                </View>

                <UnderlineTextInput
                  onRef={r => {
                    this.phone = r;
                  }}
                  onChangeText={this.nikChange}
                  blurOnSubmit={false}
                  keyboardType="number-pad"
                  placeholder="NIK"
                  placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                  inputTextColor={INPUT_TEXT_COLOR}
                  borderColor={INPUT_BORDER_COLOR}
                  focusedBorderColor={INPUT_FOCUSED_BORDER_COLOR}
                  inputContainerStyle={styles.inputContainer}
                />

                <Avatar
                  type={'ktp'}
                  callbackOpen={() => {}}
                  callbackClose={() => {}}
                  onChange={this.onKTPChange}
                  source={require('../../assets/img/profile_1.jpeg')}
                  value={this.props.photoKtp}
                />

                <InputSelectContainer
                  style={{marginBottom: 0}}
                  placeholder="Tanggal Lahir"
                  value={birth_date}
                  onPress={this.showDatePicker}
                />

                <InputSelectContainer
                  style={{marginBottom: 0}}
                  placeholder="Pilih Provinsi"
                  value={provinsi && provinsi.name}
                  onPress={this.provinceFocus}
                />

                <InputSelectContainer
                  style={{marginBottom: 0}}
                  disabled={provinsi === null}
                  placeholder="Pilih Kota/Kabupaten"
                  value={kota && kota.name}
                  onPress={this.cityFocus}
                />

                <InputSelectContainer
                  style={{marginBottom: 0}}
                  disabled={kota === null}
                  placeholder="Pilih Kecamatan"
                  value={kecamatan && kecamatan.name}
                  onPress={this.kecamatanFocus}
                />

                <InputSelectContainer
                  style={{marginBottom: 0}}
                  disabled={kecamatan === null}
                  placeholder="Pilih Kelurahan"
                  value={kelurahan && kelurahan.name}
                  onPress={this.kelurahanFocus}
                />

                <UnderlineTextInput
                  onRef={r => {
                    this.alamat = r;
                  }}
                  onChangeText={this.alamatChange}
                  returnKeyType="done"
                  blurOnSubmit={false}
                  keyboardType="default"
                  placeholder="Alamat"
                  placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                  inputTextColor={INPUT_TEXT_COLOR}
                  borderColor={INPUT_BORDER_COLOR}
                  focusedBorderColor={INPUT_FOCUSED_BORDER_COLOR}
                  inputContainerStyle={styles.inputContainer}
                />

                <View style={styles.buttonContainer}>
                  <Button
                    onPress={this.createAccount}
                    title={'Daftar'.toUpperCase()}
                  />
                </View>

                <View style={styles.separator}>
                  <View style={styles.line} />
                  {/* <Text style={styles.orText}>or</Text> */}
                  <View style={styles.line} />
                </View>

                <View style={styles.buttonsGroup}>
                  {/* <Button
                  onPress={this.createAccount}
                  color="#3b5998"
                  socialIconName="facebook-square"
                  iconColor={Colors.white}
                  title={'Sign up with Facebook'.toUpperCase()}
                />

                <View style={styles.vSpacer} />

                <Button
                  onPress={this.createAccount}
                  color="#db4437"
                  // socialIconName="google"
                  iconColor={Colors.white}
                  title={'Sign up with Google'.toUpperCase()}
                /> */}
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </ScrollView>
        <TouchableWithoutFeedback onPress={this.navigateTo('TermsConditions')}>
          <View style={styles.footer}>
            <Text style={styles.footerText}>
              By registering, you accepts our
            </Text>
            <View style={styles.termsContainer}>
              <Text style={[styles.footerText, styles.footerLink]}>
                Terms & Conditions
              </Text>
              <Text style={styles.footerText}> and </Text>
              <Text style={[styles.footerText, styles.footerLink]}>
                Privacy Policy
              </Text>
              <Text style={styles.footerText}>.</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    pitcure: state.pitcutereReducer.pitcure,
    photoKtp: state.fotoKtpReducer.foto_ktp,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchUpload: pitcure => dispatch(fetchUpload(pitcure)),
    fetchKtpUpload: pitcure => dispatch(fetchKtpUpload(pitcure)),
    fetchSignUp: params => dispatch(fetchSignUp(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpA);
