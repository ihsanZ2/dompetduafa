/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {Component} from 'react';
import {
  FlatList,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import Color from 'color';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { SafeAreaView } from "react-navigation";

// import utils
import getImgSource from '../../utils/getImgSource.js';

// import components
import CardProductDc from '../../components/cards/CardProductDc.js';
import CardDcHorizontal from '../../components/cards/CardDcHorizontal.js';
import ActionProductCardHorizontal from '../../components/cards/ActionProductCardHorizontal';
import LinkButton from '../../components/buttons/LinkButton';
import {Heading6} from '../../components/text/CustomText';
import TouchableItem from '../../components/TouchableItem';
import {connect} from 'react-redux';
import {
  fetchDataUser,
  fetchDataProvince,
  fetchDataCity,
  fetchDataSubdistrict,
  fetchDataKelurahan,
  fetchDataDistributorRegion,
  fetchDataArticle,
  fetchDataProductSearch,
  fetchDataDistributorSearch,
  fetchDataDistributorSearchByLokasi,
  fetchDataDistributorDetail,
  fetchUpload,
  fetchDataProfile,
  deleteProfile,
  firstInstallAction,
} from '../../stores/actions';

// import coloxrs
import Colors from '../../theme/colors';
import Article from '../../components/home/Article.js';

// HomeA Config
const imgHolder = require('../../assets/img/imgholder.png');

// HomeA Styles
const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  container: {
    flex: 1,
  },
  categoriesContainer: {
    paddingBottom: 5,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingTop: 16,
    paddingHorizontal: 16,
    paddingBottom: 5,
  },
  titleText: {
    fontWeight: '700',
    fontSize: 16,
  },
  viewAllText: {
    color: Colors.primaryColor,
  },
  categoriesList: {
    paddingTop: 4,
    paddingRight: 16,
    paddingLeft: 8,
  },
  cardImg: {borderRadius: 4},
  card: {
    marginLeft: 8,
    width: 104,
    height: 72,
    resizeMode: 'cover',
  },
  cardOverlay: {
    flex: 1,
    borderRadius: 4,
    backgroundColor: Color(Colors.overlayColor).alpha(0.2),
    overflow: 'hidden',
  },
  cardContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cardTitle: {
    padding: 12,
    fontWeight: '500',
    fontSize: 12,
    color: Colors.white,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  productsList: {
    paddingBottom: 16,
    // spacing = paddingHorizontal + ActionProductCard margin = 12 + 4 = 16
    paddingHorizontal: 12,
  },
  popularProductsList: {
    // spacing = paddingHorizontal + ActionProductCardHorizontal margin = 12 + 4 = 16
    paddingHorizontal: 12,
    paddingBottom: 16,
  },
});

class HomeA extends Component {
  constructor(props) {
    super(props);

    this.state = {
      clickcount: 0,
      categories: [
        {
          key: 1,
          imageUri: require('../../assets/img/pizza_3.jpg'),
          name: 'Pizza',
        },
        {
          key: 2,
          imageUri: require('../../assets/img/meat_1.jpg'),
          name: 'Grill',
        },
        {
          key: 3,
          imageUri: require('../../assets/img/spaghetti_2.jpg'),
          name: 'Pasta',
        },
        {
          key: 4,
          imageUri: require('../../assets/img/soup_1.jpg'),
          name: 'Soups',
        },
        {
          key: 5,
          imageUri: require('../../assets/img/salad_1.jpg'),
          name: 'Salads',
        },
      ],
      products: [
        {
          imageUri: require('../../assets/img/pizza_4.png'),
          name: 'Pizza Carbonara 35cm',
          price: 10.99,
          label: 'new',
        },
        {
          imageUri: require('../../assets/img/sandwich_1.png'),
          name: 'Breakfast toast sandwich',
          price: 4.99,
        },
        {
          imageUri: require('../../assets/img/cake_3.png'),
          name: 'Cake Cherries Pie',
          price: 8.49,
          discountPercentage: 10,
        },
        {
          imageUri: require('../../assets/img/soup_2.png'),
          name: 'Broccoli Soup',
          price: 6.49,
          discountPercentage: 10,
        },
      ],
      popularProducts: [
        {
          imageUri: require('../../assets/img/sandwich_2.jpg'),
          name: 'Subway sandwich',
          price: 8.49,
          quantity: 0,
          discountPercentage: 10,
        },
        {
          imageUri: require('../../assets/img/pizza_1.jpg'),
          name: 'Pizza Margarita 35cm',
          price: 10.99,
          quantity: 0,
        },
        {
          imageUri: require('../../assets/img/cake_1.jpg'),
          name: 'Chocolate cake',
          price: 4.99,
          quantity: 0,
        },
      ],
    };
  }

  componentDidMount() {
    const {province} = this.props;
    // const screen = routes[index].name
    // console.log('M',screen)
    // BackHandler.addEventListener('hardwareBackPress', () => {
    //   this.setState({clickcount: this.state.clickcount + 1});
    //   this.check();
    //   return true;
    // });
    this.props.firstInstallAction();
    this.props.fetchDataDistributorRegion();
    this.props.fetchDataArticle();
    this.props.fetchDataDistributorSearch();
    this.props.fetchProvince();
  }

  check = () => {
    const {index, routes} = this.props.navigation.dangerouslyGetState();
    const screen = routes[index].name;
    console.log('M', screen);
    // if (screen == 'Home') {
    //   if (this.state.clickcount == 1) {
    //     ToastAndroid.show('tap back again to exit the App', ToastAndroid.SHORT);
    //     setTimeout(() => {
    //       this.setState({clickcount: 0});
    //     }, 3000);
    //   } else if (this.state.clickcount == 2) {
    //     BackHandler.exitApp();
    //   }
    //   return true;
    // }else{
    //   return true
    // }
  };

  navigateTo = screen => () => {
    const {navigation} = this.props;
    navigation.navigate(screen);
  };

  onPressRemove = item => () => {
    let {quantity} = item;
    quantity -= 1;

    const {popularProducts} = this.state;
    const index = popularProducts.indexOf(item);

    if (quantity < 0) {
      return;
    }
    popularProducts[index].quantity = quantity;

    this.setState({
      popularProducts: [...popularProducts],
    });
  };

  onPressAdd = item => () => {
    const {quantity} = item;
    const {popularProducts} = this.state;

    const index = popularProducts.indexOf(item);
    popularProducts[index].quantity = quantity + 1;

    this.setState({
      popularProducts: [...popularProducts],
    });
    console.log('popular +', this.state.popularProducts);
  };

  keyExtractor = (item, index) => index.toString();

  renderCategoryItem = ({item, index}) => (
    <ImageBackground
      key={index}
      defaultSource={imgHolder}
      source={getImgSource(item.image)}
      imageStyle={styles.cardImg}
      style={styles.card}>
      <View style={styles.cardOverlay}>
        <TouchableItem
          // onPress={this.navigateTo('DistributorRegion')}
          onPress={() => {
            // this.props.fetchDataProductSearch({
            //   distributor_id: 6,
            //   page: 1,
            //   per_page: 7,
            // });
            this.props.fetchDataDistributorSearchByLokasi({
              distributor_name: item.city_name,
            });
          }}
          style={styles.cardContainer}
          // borderless
        >
          <Text style={styles.cardTitle}>
            {item.city_name + ` (${item.count})` || ''}
          </Text>
        </TouchableItem>
        {/* <Text style={styles.cardTitle}>
          {this.props.user.length > 0 ? this.props.user[1]?.name : ''}
        </Text>
        <Text style={styles.cardTitle}>
          {this.props.province?.data.length > 0
            ? this.props.province.data[1]?.province_name
            : ''}
        </Text> */}
      </View>
    </ImageBackground>
  );

  renderProductItem = ({item, index}) => (
    <CardProductDc
      onPress={this.navigateTo('Product')}
      key={index}
      imageUri={item.image}
      title={item.product_name}
      price={item.price}
      discountPercentage={item.discountPercentage}
      label={item.label}
    />
  );

  renderPopularProductItem = ({item, index}) => (
    <CardDcHorizontal
      // onPress={this.navigateTo('Product')}
      onPress={() => {
        this.props.fetchDataDistributorDetail({id: item.id});
      }}
      onPressRemove={this.onPressRemove(item)}
      onPressAdd={this.onPressAdd(item)}
      swipeoutDisabled
      key={index}
      imageUri={item.image}
      title={item.distributor_name}
      city_name={item.city_name}
      alamat={item.alamat}
      penanggung_jawab={item.penanggung_jawab}
      kontak_penanggung_jawab={item.kontak_penanggung_jawab}
      description={item.description}
      rating={item.rating}
      price={item.price}
      quantity={item.quantity}
      discountPercentage={item.discountPercentage}
      label={item.label}
    />
  );

  render() {
    const {categories, popularProducts} = this.state;
    const {distributorRegion, product, searchDistributor, route} = this.props;
    return (
      <SafeAreaView style={styles.screenContainer}>
        <StatusBar
          backgroundColor={Colors.statusBarColor}
          barStyle="dark-content"
        />
        <View style={styles.container}>
          <ScrollView>
            <View style={styles.categoriesContainer}>
              <View style={styles.titleContainer}>
                <Heading6 style={styles.titleText}>Cabang DC</Heading6>
                {/* <LinkButton
                  title="Lihat Semua"
                  titleStyle={styles.viewAllText}
                  onPress={this.navigateTo('Categories')}
                /> */}
              </View>

              <FlatList
                data={
                  distributorRegion?.data && distributorRegion?.data.length > 0
                    ? distributorRegion.data
                    : categories
                }
                horizontal
                showsHorizontalScrollIndicator={false}
                alwaysBounceHorizontal={false}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderCategoryItem}
                contentContainerStyle={styles.categoriesList}
              />
            </View>

            <View style={styles.titleContainer}>
              <Heading6 style={styles.titleText}>
                Donasi Anda berarti untuk mereka
              </Heading6>
            </View>
            <Article />

            {/* <View style={styles.titleContainer}>
              <Heading6 style={styles.titleText}>Tersedia Untuk Anda</Heading6>
            </View>

            <FlatList
              data={
                product?.data && product?.data.length > 0
                  ? product.data
                  : categories
              }
              horizontal
              showsHorizontalScrollIndicator={false}
              alwaysBounceHorizontal={false}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderProductItem}
              contentContainerStyle={styles.productsList}
            /> */}

            <View style={styles.titleContainer}>
              <Heading6 style={styles.titleText}>Distribution Center</Heading6>
              {/* <LinkButton
                title="Lihat Semua"
                titleStyle={styles.viewAllText}
                onPress={this.navigateTo('SearchResults')}
              /> */}
            </View>

            <FlatList
              data={
                searchDistributor?.data && searchDistributor?.data.length > 0
                  ? searchDistributor.data
                  : categories
              }
              keyExtractor={this.keyExtractor}
              renderItem={this.renderPopularProductItem}
              contentContainerStyle={styles.popularProductsList}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.userReducer.users,
    province: state.provinceReducer.province,
    distributorRegion: state.distributorRegionReducer.distributorRegion,
    article: state.articleReducer.article,
    product: state.product_searchReducer.product_search,
    searchDistributor: state.distributorSearchReducer.distributor_search,
    profile: state.profileReducer.profile,
    // loginStatus: state.loginReducer.login,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataUser: arg => dispatch(fetchDataUser(arg)),
    fetchProvince: () => dispatch(fetchDataProvince()),
    fetchDataCity: province_id => dispatch(fetchDataCity(province_id)),
    fetchDataSubdistrict: city_id => dispatch(fetchDataSubdistrict(city_id)),
    fetchDataKelurahan: city_id => dispatch(fetchDataKelurahan(city_id)),
    fetchDataDistributorRegion: () => dispatch(fetchDataDistributorRegion()),
    fetchDataArticle: () => dispatch(fetchDataArticle()),
    fetchDataProductSearch: product_search =>
      dispatch(fetchDataProductSearch(product_search)),
    fetchDataDistributorSearch: product_search =>
      dispatch(fetchDataDistributorSearch(product_search)),
    fetchDataDistributorSearchByLokasi: product_search =>
      dispatch(fetchDataDistributorSearchByLokasi(product_search)),
    fetchDataDistributorDetail: distributor =>
      dispatch(fetchDataDistributorDetail(distributor)),
    fetchUpload: pitcure => dispatch(fetchUpload(pitcure)),
    fetchDataProfile: () => dispatch(fetchDataProfile()),
    deleteProfile: () => dispatch(deleteProfile()),
    firstInstallAction: () => dispatch(firstInstallAction()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeA);
