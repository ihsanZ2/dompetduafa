/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {Component} from 'react';
import {
  FlatList,
  I18nManager,
  ImageBackground,
  Keyboard,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {connect} from 'react-redux';

import {
  fetchDataUser,
  fetchDataProvince,
  fetchDataCity,
  fetchDataSubdistrict,
  fetchDataKelurahan,
  fetchDataDistributorRegion,
  fetchDataArticle,
  fetchDataProductSearch,
  fetchDataDistributorSearch,
  fetchDataDistributorSearchByLokasi,
  fetchDataDistributorDetail,
} from '../../stores/actions';

import Color from 'color';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// import utils
import getImgSource from '../../utils/getImgSource.js';

// import components
import TouchableItem from '../../components/TouchableItem';
import {Heading6} from '../../components/text/CustomText';
import CardDcHorizontal from '../../components/cards/CardDcHorizontal.js';

// import colors
import Colors from '../../theme/colors';

// DistributorRegion Config
const isRTL = I18nManager.isRTL;
const SEARCH_ICON = 'magnify';
const imgHolder = require('../../assets/img/imgholder.png');

// DistributorRegion Styles
const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  container: {
    flex: 1,
  },
  titleContainer: {
    paddingHorizontal: 16,
  },
  titleText: {
    paddingTop: 16,
    paddingBottom: 8,
    fontWeight: '700',
    textAlign: 'left',
  },
  inputContainer: {
    marginHorizontal: 16,
    paddingBottom: 10,
  },
  titleDescription: {
    flex: 1,
    fontWeight: '600',
    fontSize: 12,
    color: Colors.primaryText,
    letterSpacing: 0.15,
    textAlign: 'left',
  },
  textInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.12)',
    paddingLeft: 8,
    paddingRight: 51,
    height: 46,
    fontSize: 16,
    textAlignVertical: 'center',
    textAlign: isRTL ? 'right' : 'left',
  },
  searchButtonContainer: {
    position: 'absolute',
    top: 4,
    right: 4,
    borderRadius: 4,
    backgroundColor: Colors.primaryColor,
    overflow: 'hidden',
  },
  searchButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 38,
    height: 38,
  },
  categoriesList: {
    paddingBottom: 10,
  },
  cardImg: {borderRadius: 4},
  card: {
    marginVertical: 6,
    // marginHorizontal: 16,
    height: 100,
    resizeMode: 'cover',
  },
  cardOverlay: {
    flex: 1,
    borderRadius: 4,
    backgroundColor: Color(Colors.overlayColor).alpha(0.2),
    overflow: 'hidden',
  },
  cardContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cardTitle: {
    padding: 16,
    fontWeight: '700',
    fontSize: 18,
    color: Colors.white,
    textShadowColor: 'rgba(0, 0, 0, 0.88)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
});

// DistributorRegion
// export default class DistributorRegion extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       categories: [
//         {
//           key: 1,
//           imageUri: require('../../assets/img/pizza_3.jpg'),
//           name: 'Pizza',
//         },
//         {
//           key: 2,
//           imageUri: require('../../assets/img/meat_1.jpg'),
//           name: 'Grill',
//         },
//         {
//           key: 3,
//           imageUri: require('../../assets/img/spaghetti_2.jpg'),
//           name: 'Pasta',
//         },
//         {
//           key: 4,
//           imageUri: require('../../assets/img/soup_1.jpg'),
//           name: 'Soups',
//         },
//         {
//           key: 5,
//           imageUri: require('../../assets/img/salad_1.jpg'),
//           name: 'Salads',
//         },
//         {
//           key: 6,
//           imageUri: require('../../assets/img/cake_2.jpg'),
//           name: 'Dessert',
//         },
//       ],
//     };
//   }
// }

export class DistributorRegion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [
        {
          key: 1,
          imageUri: require('../../assets/img/pizza_3.jpg'),
          name: 'Pizza',
        },
        {
          key: 2,
          imageUri: require('../../assets/img/meat_1.jpg'),
          name: 'Grill',
        },
        {
          key: 3,
          imageUri: require('../../assets/img/spaghetti_2.jpg'),
          name: 'Pasta',
        },
        {
          key: 4,
          imageUri: require('../../assets/img/soup_1.jpg'),
          name: 'Soups',
        },
        {
          key: 5,
          imageUri: require('../../assets/img/salad_1.jpg'),
          name: 'Salads',
        },
        {
          key: 6,
          imageUri: require('../../assets/img/cake_2.jpg'),
          name: 'Dessert',
        },
      ],
    };
  }
  navigateTo = screen => () => {
    const {navigation} = this.props;

    Keyboard.dismiss();

    navigation.navigate(screen);
  };

  keyExtractor = (item, index) => index.toString();

  renderCategoryItem = ({item, index}) => (
    <View style={{paddingHorizontal: 5}}>
      <CardDcHorizontal
        // onPress={this.navigateTo('Product')}
        onPress={() => {
          this.props.fetchDataDistributorDetail({id: item.id});
        }}
        onPressRemove={() => {}}
        onPressAdd={() => {}}
        swipeoutDisabled
        key={index}
        imageUri={item.image}
        title={item.distributor_name}
        city_name={item.city_name}
        alamat={item.alamat}
        penanggung_jawab={item.penanggung_jawab}
        kontak_penanggung_jawab={item.kontak_penanggung_jawab}
        description={item.description}
        rating={item.rating}
        price={item.price}
        quantity={item.quantity}
        discountPercentage={item.discountPercentage}
        label={item.label}
      />
    </View>

    // <View
    //   style={{
    //     marginTop: 20,
    //     marginBottom: 2.5,
    //     marginHorizontal: 16,
    //     resizeMode: 'cover',
    //     borderWidth: 1,
    //     borderColor: 'rgba(0, 0, 0, 0.08)',
    //     paddingHorizontal: 6,
    //     paddingVertical: 6,
    //     borderRadius: 5,
    //   }}>
    //   <TouchableWithoutFeedback
    //     // onPress={this.navigateTo('Category')}
    //     // style={{marginHorizontal:0,marginVertical:0}}
    //     borderless>
    //     <View>
    //       <ImageBackground
    //         key={index}
    //         defaultSource={imgHolder}
    //         source={getImgSource(item.image)}
    //         imageStyle={styles.cardImg}
    //         style={styles.card}>
    //         {/* <View style={styles.cardOverlay}>
    //       <TouchableItem
    //         onPress={this.navigateTo('Category')}
    //         style={styles.cardContainer}
    //         // borderless
    //       >
    //         <Text style={styles.cardTitle}>{item.name}</Text>
    //       </TouchableItem>
    //     </View> */}
    //       </ImageBackground>
    //       <View>
    //         <Text numberOfLines={2} style={styles.titleDescription}>
    //           Nama Distributor : {item.distributor_name}
    //         </Text>
    //         <Text numberOfLines={2} style={styles.titleDescription}>
    //           Provinsi : {item.province_name}
    //         </Text>
    //         <Text numberOfLines={2} style={styles.titleDescription}>
    //           Kota/Kabupaten : {item.city_name}
    //         </Text>
    //         <Text numberOfLines={2} style={styles.titleDescription}>
    //           Penanggung Jawab : {item.city_name}
    //         </Text>
    //         <Text numberOfLines={2} style={styles.titleDescription}>
    //           Kontak Penanggung Jawab : {item.city_name}
    //         </Text>
    //       </View>
    //     </View>
    //   </TouchableWithoutFeedback>
    // </View>
  );

  render() {
    const {categories} = this.state;
    const {searchDistributor} = this.props;
    return (
      <SafeAreaView style={styles.screenContainer}>
        <StatusBar
          backgroundColor={Colors.statusBarColor}
          barStyle="dark-content"
        />

        {/* <View style={styles.titleContainer}>
          <Heading6 style={styles.titleText}>Search</Heading6>
        </View>

        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Food name or description..."
            returnKeyType="search"
            maxLength={50}
            style={styles.textInput}
          />
          <View style={styles.searchButtonContainer}>
            <TouchableItem
              onPress={this.navigateTo('SearchResults')}
              // borderless
            >
              <View style={styles.searchButton}>
                <Icon
                  name={SEARCH_ICON}
                  size={23}
                  color={Colors.onPrimaryColor}
                />
              </View>
            </TouchableItem>
          </View>
        </View> */}

        <View style={styles.container}>
          <FlatList
            data={
              searchDistributor?.data && searchDistributor?.data.length > 0
                ? searchDistributor.data
                : categories
            }
            showsHorizontalScrollIndicator={false}
            alwaysBounceHorizontal={false}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderCategoryItem}
            contentContainerStyle={styles.categoriesList}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.userReducer.users,
    province: state.provinceReducer.province,
    distributorRegion: state.distributorRegionReducer.distributorRegion,
    article: state.articleReducer.article,
    product: state.product_searchReducer.product_search,
    searchDistributor:
      state.distributorSearchByLokasiReducer.distributor_search_by_lokasi,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataUser: arg => dispatch(fetchDataUser(arg)),
    fetchProvince: () => dispatch(fetchDataProvince()),
    fetchDataCity: province_id => dispatch(fetchDataCity(province_id)),
    fetchDataSubdistrict: city_id => dispatch(fetchDataSubdistrict(city_id)),
    fetchDataKelurahan: city_id => dispatch(fetchDataKelurahan(city_id)),
    fetchDataDistributorRegion: () => dispatch(fetchDataDistributorRegion()),
    fetchDataArticle: () => dispatch(fetchDataArticle()),
    fetchDataProductSearch: product_search =>
      dispatch(fetchDataProductSearch(product_search)),
    fetchDataDistributorSearch: product_search =>
      dispatch(fetchDataDistributorSearch(product_search)),
    fetchDataDistributorSearchByLokasi: product_search =>
      dispatch(fetchDataDistributorSearchByLokasi(product_search)),
    fetchDataDistributorDetail: distributor =>
      dispatch(fetchDataDistributorDetail(distributor)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DistributorRegion);
