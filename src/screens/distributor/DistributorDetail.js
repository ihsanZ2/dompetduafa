/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {useState, useRef} from 'react';
import {
  I18nManager,
  Image,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  FlatList,
  View,
  Animated,
} from 'react-native';
import Swiper from 'react-native-swiper';
import {connect} from 'react-redux';

// import utils
import getImgSource from '../../utils/getImgSource.js';

// import components
import Button from '../../components/buttons/Button';
import {
  Caption,
  Heading5,
  Heading6,
  SmallText,
} from '../../components/text/CustomText';
import Icon from '../../components/icon/Icon';
import SizePicker from '../../components/pickers/SizePicker';
import TouchableItem from '../../components/TouchableItem';
import ListProduct from '../../components/product/ListProduct.js';

// import colors
import Colors from '../../theme/colors';

import {deleteProductSearch, fetchCart} from '../../stores/actions/index.js';

// ProductA Config
const isRTL = I18nManager.isRTL;
const IOS = Platform.OS === 'ios';
const MINUS_ICON = IOS ? 'ios-remove' : 'md-remove';
const PLUS_ICON = IOS ? 'ios-add' : 'md-add';
const FAVORITE_ICON = IOS ? 'ios-star' : 'md-star';
const CLOSE_ICON = IOS ? 'ios-close' : 'md-close';
const imgHolder = require('../../assets/img/imgholder.png');

const HEADER_MAX_HEIGHT = 240;
const HEADER_MIN_HEIGHT = 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

// ProductA Styles
const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  swiperContainer: {
    width: '100%',
    height: 228,
  },
  paginationStyle: {
    bottom: 12,
    transform: [{scaleX: isRTL ? -1 : 1}],
  },
  dot: {backgroundColor: Colors.background},
  activeDot: {backgroundColor: Colors.primaryColor},
  slideImg: {
    width: '100%',
    height: 228,
    resizeMode: 'cover',
  },
  topButton: {
    position: 'absolute',
    top: 16,
    borderRadius: 18,
    backgroundColor: Colors.background,
  },
  left: {left: 16},
  right: {right: 16},
  buttonIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 36,
    height: 36,
  },
  favorite: {
    backgroundColor: Colors.secondaryColor,
  },
  descriptionContainer: {
    paddingHorizontal: 16,
    marginBottom: 10,
  },
  productTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 7,
  },
  productTitle: {
    fontWeight: '700',
  },
  priceText: {
    fontWeight: '700',
    fontSize: 18,
    color: Colors.primaryColor,
  },
  shortDescription: {
    paddingBottom: 8,
    textAlign: 'left',
  },
  pickerGroup: {
    marginTop: 24,
  },
  pickerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  caption: {
    width: 80,
    textAlign: 'left',
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  amountButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  quantity: {
    top: -1,
    paddingHorizontal: 20,
    fontSize: 18,
    color: Colors.black,
    textAlign: 'center',
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 8,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: Colors.secondaryColor,
  },
  bottomButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    paddingVertical: 9,
    paddingHorizontal: 24,
  },
  buttonPriceContainer: {
    position: 'absolute',
    top: 0,
    left: 40,
    height: 48,
    justifyContent: 'center',
  },
  buttonPriceText: {
    fontSize: 16,
    lineHeight: 18,
    color: Colors.onPrimaryColor,
  },
  popularProductsList: {
    // spacing = paddingHorizontal + ActionProductCardHorizontal margin = 12 + 4 = 16
    paddingHorizontal: 12,
    paddingBottom: 16,
  },
  saveArea: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#402583',
    backgroundColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
    elevation: 1,
    borderRadius: 10,
    marginHorizontal: 12,
    marginTop: 12,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.primaryColor,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  headerBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  topBar: {
    marginTop: 40,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 20,
  },
  avatar: {
    height: 54,
    width: 54,
    resizeMode: 'contain',
    borderRadius: 54 / 2,
  },
  fullNameText: {
    fontSize: 16,
    marginLeft: 24,
  },
});

function DistributorDetail(props) {
  const [cartArr, setCartArr] = useState([]);
  const [favorite, setFavorite] = useState(false);
  const [state, setState] = useState({
    product: {
      images: [
        require('../../assets/img/pizza_3.jpg'),
        require('../../assets/img/pizza_1.jpg'),
        require('../../assets/img/pizza_2.jpg'),
      ],
      name: 'Pizza Carbonara',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
      price: 10.9,
      quantity: 1,
      servingSize: 1,
      sideDish: 20,
      total: 10.9,
    },
    cartArr: [],
    favorite: false,
  });

  const scrollY = useRef(new Animated.Value(0)).current;

  const headerTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',
  });
  const imageTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const titleScale = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, 0.9],
    extrapolate: 'clamp',
  });
  const titleTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, -45],
    extrapolate: 'clamp',
  });

  const goBack = () => {
    const {navigation, deleteProductSearch} = props;
    deleteProductSearch();
    navigation.pop();
  };

  const onPressAddToFavorites = () => {
    const {favorite} = state;

    setState({
      favorite: !favorite,
    });
  };

  const onPressIncreaseAmount = () => {
    const {product} = state;
    let {quantity} = product;
    const {servingSize} = product;

    quantity += 1;
    product.quantity = quantity;

    const total = quantity * product.price * servingSize;
    product.total = total;

    setState({
      product,
    });
  };

  const onPressDecreaseAmount = () => {
    const {product} = state;
    let {quantity} = product;
    const {servingSize} = product;

    quantity -= 1;
    quantity = quantity < 1 ? 1 : quantity;
    product.quantity = quantity;

    const total = quantity * product.price * servingSize;
    product.total = total;

    setState({
      product,
    });
  };

  const setServingSize = servingSize => () => {
    const {product} = state;
    const {quantity} = product;

    product.servingSize = servingSize;

    const total = quantity * product.price * servingSize;
    product.total = total;

    setState({
      product,
    });
  };

  const setSideDish = sideDish => () => {
    const {product} = state;
    product.sideDish = sideDish;

    setState({
      product,
    });
  };

  const listProduct = (item, index) => {
    return (
      <ListProduct
        onPress={handleCallBack}
        onPressRemove={() => {}}
        onPressAdd={() => {}}
        swipeoutDisabled
        key={index}
        imageUri={item.image}
        item={item}
      />
    );
  };

  const keyExtractor = (item, index) => index.toString();

  const handleCallBack = params => {
    if (state.cartArr.includes(params)) {
      let arr = [...state.cartArr];
      let newA = arr.filter(item => item !== params);
      setState({cartArr: newA});
    } else {
      setState({cartArr: [...state.cartArr, params]});
    }
  };

  const addToCart = () => {
    props.fetchCart({
      product_ids: state.cartArr,
    });
  };

  // render() {
  const {product} = state;
  const {distributorDetail, products} = props;
  // const {images, price, description, quantity, servingSize, sideDish, total} = product;

  return (
    <SafeAreaView style={styles.saveArea}>
      <Animated.ScrollView
        contentContainerStyle={{paddingTop: HEADER_MAX_HEIGHT,paddingHorizontal:10}}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: true},
        )}>
        <View style={styles.descriptionContainer}>
          <View style={styles.productTitleContainer}>
            <Heading6 style={styles.productTitle}>
              {distributorDetail.data.distributor_name}
            </Heading6>
          </View>
        </View>
        <View style={{paddingHorizontal:10,marginBottom:5}}>
          <SmallText numberOfLines={2} style={styles.shortDescription}>
            No. Telepon DC : {distributorDetail.data.no_telp || ''}
          </SmallText>
          <SmallText style={styles.shortDescription}>
            Alamat : {distributorDetail.data.alamat || ''}
          </SmallText>
        </View>
        {products.data && products.data.map(listProduct)}
      </Animated.ScrollView>
      <Animated.View
        style={[styles.header, {transform: [{translateY: headerTranslateY}]}]}>
        <Animated.Image
          style={[
            styles.headerBackground,
            {
              opacity: imageOpacity,
              transform: [{translateY: imageTranslateY}],
            },
          ]}
          defaultSource={imgHolder}
          source={getImgSource(distributorDetail?.data?.image)}
        />
      </Animated.View>
      <Animated.View
        style={[
          styles.topBar,
          {
            transform: [{scale: titleScale}, {translateY: titleTranslateY}],
          },
        ]}>
        <Text style={styles.title}>{distributorDetail.data.city_name}</Text>
      </Animated.View>
      <View style={styles.bottomButtonContainer}>
        <Button
          onPress={addToCart}
          title={
            props.loginStatus?.data?.access_token
              ? 'Lanjutkan'.toUpperCase()
              : 'Login'.toUpperCase()
          }
        />
        <View style={styles.buttonPriceContainer}>
          <Text style={styles.buttonPriceText} />
        </View>
      </View>
      {/* <StatusBar
        backgroundColor={Colors.statusBarColor}
        barStyle="dark-content"
      />
      <View style={styles.swiperContainer}>
        <Image
          defaultSource={imgHolder}
          source={getImgSource(distributorDetail?.data?.image)}
          style={styles.slideImg}
        />

        <View style={[styles.topButton, styles.left]}>
          <TouchableItem onPress={goBack} borderless>
            <View style={styles.buttonIconContainer}>
              <Icon name={CLOSE_ICON} size={22} color={Colors.secondaryText} />
            </View>
          </TouchableItem>
        </View>
      </View>

      <View style={styles.descriptionContainer}>
        <View style={styles.productTitleContainer}>
          <Heading6 style={styles.productTitle}>
            {distributorDetail.data.distributor_name}
          </Heading6>
        </View>
      </View>
      <View style={{flex: 1}}>
        <Animated.FlatList
          ListHeaderComponent={
            <View>
              <SmallText numberOfLines={2} style={styles.shortDescription}>
                No. Telepon DC : {distributorDetail.data.no_telp || ''}
              </SmallText>
              <SmallText style={styles.shortDescription}>
                Alamat : {distributorDetail.data.alamat || ''}
              </SmallText>
            </View>
          }
          data={products.data}
          keyExtractor={keyExtractor}
          renderItem={listProduct}
          contentContainerStyle={styles.popularProductsList}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}
        />
      </View>

      <View style={styles.bottomButtonContainer}>
        <Button
          onPress={addToCart}
          title={
            props.loginStatus?.data?.access_token
              ? 'Lanjutkan'.toUpperCase()
              : 'Login'.toUpperCase()
          }
        />
        <View style={styles.buttonPriceContainer}>
          <Text style={styles.buttonPriceText} />
        </View>
      </View> */}
    </SafeAreaView>
  );
}
// }

const mapStateToProps = state => {
  return {
    loginStatus: state.loginReducer.login,
    province: state.provinceReducer.province,
    distributorRegion: state.distributorRegionReducer.distributorRegion,
    article: state.articleReducer.article,
    products: state.product_searchReducer.product_search,
    searchDistributor: state.distributorSearchReducer.distributor_search,
    distributorDetail: state.distributorDetailReducer.distributor_detail,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteProductSearch: () => dispatch(deleteProductSearch()),
    fetchCart: cart => dispatch(fetchCart(cart)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DistributorDetail);

