/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {useState, useEffect} from 'react';
import {Platform} from 'react-native';
import {
  NavigationContainer,
  useNavigationContainerRef,
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useReduxDevToolsExtension} from '@react-navigation/devtools';
import {useRef} from 'react';
import {navigationRef} from '../navigation/RootNavigation';
import {connect} from 'react-redux';
import axios from 'axios';
import {styles} from '../components/text/CustomText';

import {fetchDataProfile} from '../stores/actions';

// import components
import HeaderIconButton from '../components/navigation/HeaderIconButton';

// import Onboarding screen
import Onboarding from '../screens/onboarding/OnboardingA';

// import Welcome screen
import Welcome from '../screens/welcome/WelcomeA';

// import SignUp screen
import SignUp from '../screens/signup/SignUpA';

// import Verification screen
import Verification from '../screens/verification/VerificationA';

// import SignIn screen
import SignIn from '../screens/signin/SignInA';

// import ForgotPassword screen
import ForgotPassword from '../screens/forgotpassword/ForgotPasswordA';

// import TermsConditions screen
import TermsConditions from '../screens/terms/TermsConditionsA';

// import HomeNavigator
import HomeNavigator from './HomeNavigatorA';

// import Product screen
import Product from '../screens/product/ProductA';

// import Categories screen
import Categories from '../screens/categories/CategoriesA';
import Category from '../screens/categories/CategoryA';

// import Search results screen
import SearchResults from '../screens/search/SearchResultsA';

// import Checkout screen
import Checkout from '../screens/checkout/CheckoutA';

// import EditProfile screen
import EditProfile from '../screens/profile/EditProfileA';

// import DeliveryAddress screen
import DeliveryAddress from '../screens/address/DeliveryAddressA';

// import AddAddress screen
import AddAddress from '../screens/address/AddAddressA';

// import EditAddress screen
import EditAddress from '../screens/address/EditAddressA';

// import Payment screen
import PaymentMethod from '../screens/payment/PaymentMethodA';

// import AddCreditCard screen
import AddCreditCard from '../screens/payment/AddCreditCardA';

// import Notifications screen
import Notifications from '../screens/notifications/Notifications';

// import Orders screen
import Orders from '../screens/orders/OrdersA';

// import AboutUs screen
import AboutUs from '../screens/about/AboutUsA';

// distibutor Region screen
import DistributorRegion from '../screens/distributor/DistributorRegion';

// distributor detail
import DistributorDetail from '../screens/distributor/DistributorDetail';

// import colors
import Colors from '../theme/colors';
import WebViewContainer from '../components/webview/WebViewContainer';

// MainNavigatorA Config
const SAVE_ICON = Platform.OS === 'ios' ? 'ios-checkmark' : 'md-checkmark';

// create stack navigator
const Stack = createStackNavigator();

// MainNavigatorA
function MainNavigatorA(props) {
  // const navigationRef = useNavigationContainerRef();
  const [firstDownload, setFirstDownload] = useState(false);
  const routeNameRef = useRef();

  useEffect(() => {
    // if (props && props.loginStatus) {
    //   axios.defaults.headers.common['Authorization'] =
    //     props.loginStatus.data.access_token;
    // }
    // if(props && props.loginStatus){
    // let token = props.loginStatus?.data.access_token;
    // if (token != props.loginStatus.data.access_token) {
    //   axios.defaults.headers.common['Authorization'] =
    //     props.loginStatus.data.access_token;
    //   props.fetchDataProfile();
    // } else {
    //   [(axios.defaults.headers.common['Authorization'] = token)];
    //   props.fetchDataProfile();
    // }}
    return () => {};
  }, []);

  useReduxDevToolsExtension(navigationRef);
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        routeNameRef.current = navigationRef.current.getCurrentRoute().name;
      }}
      onStateChange={async () => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        if (props && props.loginStatus) {
          let token = props.loginStatus?.data.access_token;
          axios.defaults.headers.common.Authorization = token;
        } else {
          axios.defaults.headers.common.Authorization = null;
        }
        console.log(
          'NAVIGATION',
          `PREV (${previousRouteName}) -- CRNT (${currentRouteName})`,
        );

        if (previousRouteName !== currentRouteName) {
          // The line below uses the expo-firebase-analytics tracker
          // https://docs.expo.io/versions/latest/sdk/firebase-analytics/
          // Change this line to use another Mobile analytics SDK
          // await Analytics.setCurrentScreen(currentRouteName);
          await currentRouteName;
        }

        // Save the current route name for later comparison
        routeNameRef.current = currentRouteName;
      }}>
      <Stack.Navigator
        screenOptions={{
          cardOverlayEnabled: false,
          headerStyle: {
            elevation: 1,
            shadowOpacity: 0,
          },
          headerBackTitleVisible: false,
          headerTitleStyle: {...styles.subtitle1, fontWeight: 'bold'},
          headerTintColor: Colors.onBackground,
          headerTitleAlign: 'center',
        }}>
        {props.firstInstall && (
          <Stack.Screen
            name="Onboarding"
            component={Onboarding}
            options={{headerShown: false}}
          />
        )}
        <Stack.Screen
          name="HomeNavigator"
          component={HomeNavigator}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{
            title: 'DAFTAR AKUN',
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
            },
          }}
        />
        {/* <Stack.Screen
          name="Verification"
          component={Verification}
          options={{headerShown: false}}
        /> */}
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{
            title: 'LOGIN',
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
            },
          }}
        />
        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPassword}
          options={{
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
            },
            title: 'Forgot Password?',
          }}
        />
        <Stack.Screen
          name="TermsConditions"
          component={TermsConditions}
          options={{
            title: 'Terms and Conditions',
          }}
        />
        <Stack.Screen
          name="DistributorRegion"
          component={DistributorRegion}
          options={{
            title: 'Cabang DC',
          }}
        />
        <Stack.Screen
          name="DistributorDetail"
          component={DistributorDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Categories"
          component={Categories}
          options={{
            title: 'All Categories',
          }}
        />
        <Stack.Screen
          name="Category"
          component={Category}
          options={{
            title: 'Pizza',
          }}
        />
        <Stack.Screen
          name="Product"
          component={Product}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SearchResults"
          component={SearchResults}
          options={{
            title: 'Search Results',
          }}
        />
        <Stack.Screen
          name="Checkout"
          component={Checkout}
          options={{
            title: 'Checkout',
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
            },
          }}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={({navigation}) => ({
            title: 'Edit Profile',
            headerRight: () => (
              <HeaderIconButton
                onPress={() => navigation.goBack()}
                name={SAVE_ICON}
                color={Colors.primaryColor}
              />
            ),
          })}
        />
        <Stack.Screen
          name="DeliveryAddress"
          component={DeliveryAddress}
          options={({navigation}) => ({
            title: 'Delivery Address',
            headerRight: () => (
              <HeaderIconButton
                onPress={() => navigation.goBack()}
                name={SAVE_ICON}
                color={Colors.primaryColor}
              />
            ),
          })}
        />
        <Stack.Screen
          name="AddAddress"
          component={AddAddress}
          options={{
            title: 'Add New Address',
          }}
        />
        <Stack.Screen
          name="EditAddress"
          component={EditAddress}
          options={{
            title: 'Edit Address',
          }}
        />
        <Stack.Screen
          name="PaymentMethod"
          component={PaymentMethod}
          options={({navigation}) => ({
            title: 'Payment Method',
            headerRight: () => (
              <HeaderIconButton
                onPress={() => navigation.goBack()}
                name={SAVE_ICON}
                color={Colors.primaryColor}
              />
            ),
          })}
        />
        <Stack.Screen
          name="AddCreditCard"
          component={AddCreditCard}
          options={{
            title: 'Add Credit Card',
          }}
        />
        <Stack.Screen
          name="Notifications"
          component={Notifications}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Orders"
          component={Orders}
          options={{
            title: 'My Orders',
          }}
        />
        <Stack.Screen
          name="AboutUs"
          component={AboutUs}
          options={{
            title: 'About Us',
          }}
        />
        <Stack.Screen
          name="WebViewArticle"
          component={WebViewContainer}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = state => {
  return {
    loginStatus: state.loginReducer.login,
    firstInstall: state.firstInstallReducer.firstInstall,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDataProfile: () => dispatch(fetchDataProfile()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigatorA);
