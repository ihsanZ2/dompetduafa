/**
 * Dompet Duafa - React Native Template
 *
 * @format
 * @flow
 */

// import dependencies
import React, {useEffect} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Reactotron from 'reactotron-react-native';
import {connect} from 'react-redux';

// import components
import TabBadgeIcon from '../components/navigation/TabBadgeIcon';

// import Home screen
import Home from '../screens/home/HomeA';

// import Search screen
import Search from '../screens/search/Search';

// import Favorites screen
import Favorites from '../screens/favorites/FavoritesA';

// import Cart screen
import Cart from '../screens/cart/CartA';

// import Settings screen
import Settings from '../screens/settings/SettingsA';

// login screeen
import SignIn from '../screens/signin/SignInA';

// register
import SignUp from '../screens/signup/SignUpA';

// Welcom
import Welcome from '../screens/welcome/WelcomeA';

// import colors
import Colors from '../theme/colors';
import Notifications from '../screens/notifications/Notifications';

// HomeNavigator Config

type Props = {
  color: string,
  focused: string,
  size: number,
};

// create bottom tab navigator
const Tab = createBottomTabNavigator();

// HomeNavigator
function HomeNavigator(props) {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      backBehavior="initialRoute"
      screenOptions={({route}) => ({
        tabBarIcon: ({color, focused, size}: Props) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = `home${focused ? '' : '-outline'}`;
          } else if (route.name === 'Search') {
            iconName = 'magnify';
          } else if (route.name === 'Welcome') {
            iconName = `login${focused ? '' : '-variant'}`;
          }
          // else if (route.name === 'Settings') {
          //   iconName = `settings${focused ? '' : '-outline'}`;
          // }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: Colors.primaryColor,
        inactiveTintColor: Colors.secondaryText,
        showLabel: false, // hide labels
        style: {
          backgroundColor: Colors.surface, // TabBar background
        },
      }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Search" component={Search} />
      <Tab.Screen
        name="Notifications"
        component={Notifications}
        options={{
          tabBarIcon: (props) => (
            <TabBadgeIcon
              name={`md-notifications${props.focused ? '' : '-outline'}`}
              badgeCount={5}
              {...props}
            />
          ),
        }}
      />
      {!props.loginStatus?.data?.access_token ? (
        <Tab.Screen name="Welcome" component={Welcome} />
      ) : (
        <Tab.Screen
          name="Settings"
          component={Settings}
          options={{
            tabBarIcon: (props) => (
              <TabBadgeIcon
                name={`person${props.focused ? '' : '-outline'}`}
                badgeCount={5}
                {...props}
              />
            ),
          }}
        />
      )}
    </Tab.Navigator>
  );
}

const mapStateToProps = state => {
  return {
    loginStatus: state.loginReducer.login,
  };
};

export default connect(mapStateToProps, null)(HomeNavigator);
